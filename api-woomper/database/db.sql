CREATE DATABASE woomper_db;

USE woomper_db;

CREATE TABLE Roles(
    IDRol INT AUTO_INCREMENT PRIMARY KEY,
    Rol VARCHAR(256)
);

CREATE TABLE Usuarios(
    IDUsuario INT AUTO_INCREMENT PRIMARY KEY,
    Imagen VARCHAR(256),
    NombreUsuario VARCHAR(256),
    ApellidoUsuario VARCHAR(256),
    DireccionUsuario VARCHAR(256),
    TelefonoUsuario VARCHAR(256),
    CorreoUsuario VARCHAR(256),
    Usuario VARCHAR(256),
    Contrasena VARCHAR(256),
    Estado INT,
    Codigo VARCHAR(256),
    IDRol INT,
    FOREIGN KEY (IDRol) REFERENCES Roles (IDRol) ON DELETE CASCADE
);

CREATE TABLE Sucursales(
    IDSucursal INT AUTO_INCREMENT PRIMARY KEY,
    Sucursal VARCHAR(256),
    Direccion VARCHAR(256),
    Telefono VARCHAR(256),
    Ubicacion VARCHAR(256)
);

CREATE TABLE Categorias(
    IDCategoria INT AUTO_INCREMENT PRIMARY KEY,
    Categoria VARCHAR(256)
);

CREATE TABLE SubCategorias(
    IDSubCategoria INT AUTO_INCREMENT PRIMARY KEY,
    SubCategoria VARCHAR(256),
    IDCategoria INT,
    FOREIGN KEY (IDCategoria) REFERENCES Categorias (IDCategoria) ON DELETE CASCADE
);

CREATE TABLE Proveedores(
    IDProveedor INT AUTO_INCREMENT PRIMARY KEY,
    Proveedor VARCHAR(256),
    Direccion VARCHAR(256),
    Telefono VARCHAR(256)
);

CREATE TABLE Productos(
    IDProducto INT AUTO_INCREMENT PRIMARY KEY,
    Imagen VARCHAR(256),
    Producto VARCHAR(256),
    Precio DECIMAL(10,2),
    Cantidad INT,
    Fecha VARCHAR(256),
    Estado VARCHAR(256),
    IDCategoria INT,
    IDSubCategoria INT,
    IDProveedor INT,
    IDSucursal INT,
    FOREIGN KEY (IDCategoria) REFERENCES Categorias (IDCategoria) ON DELETE CASCADE,
    FOREIGN KEY (IDSubCategoria) REFERENCES SubCategorias (IDSubCategoria) ON DELETE CASCADE,
    FOREIGN KEY (IDProveedor) REFERENCES Proveedores (IDProveedor) ON DELETE CASCADE,
    FOREIGN KEY (IDSucursal) REFERENCES Sucursales (IDSucursal) ON DELETE CASCADE
);

CREATE TABLE Comentarios(
    IDComentario INT AUTO_INCREMENT PRIMARY KEY,
    Comentario VARCHAR(256),
    Estrellas INT,
    IDProducto INT,
    IDUsuario INT,
    FOREIGN KEY (IDProducto) REFERENCES Productos (IDProducto) ON DELETE CASCADE,
    FOREIGN KEY (IDUsuario) REFERENCES Usuarios (IDUsuario) ON DELETE CASCADE
);

CREATE TABLE Estados(
    IDEstado INT AUTO_INCREMENT PRIMARY KEY,
    Estado VARCHAR(256),
    Porcentaje INT
);

CREATE TABLE Pedidos(
    IDPedido INT AUTO_INCREMENT PRIMARY KEY,
    CodigoPedido VARCHAR(256),
    IDEstado INT,
    IDUsuario INT,
    FOREIGN KEY (IDEstado) REFERENCES Estados (IDEstado) ON DELETE CASCADE,
    FOREIGN KEY (IDUsuario) REFERENCES Usuarios (IDUsuario) ON DELETE CASCADE
);

CREATE TABLE Facturas(
    IDFactura INT AUTO_INCREMENT PRIMARY KEY,
    CodigoFactura VARCHAR(256),
    Total DECIMAL(10,2),
    IDPedido INT,
    FOREIGN KEY (IDPedido) REFERENCES Pedidos (IDPedido) ON DELETE CASCADE  
);

CREATE TABLE DetalleFacturas(
    IDDetalleFactura INT AUTO_INCREMENT PRIMARY KEY,
    IDProducto INT,
    Cantidad INT,
    SubTotal DECIMAL(10,2),
    IDFactura INT,
    FOREIGN KEY (IDProducto) REFERENCES Productos (IDProducto) ON DELETE CASCADE,
    FOREIGN KEY (IDFactura) REFERENCES Facturas (IDFactura) ON DELETE CASCADE
);

CREATE TABLE Cupones(
    IDCupon INT AUTO_INCREMENT PRIMARY KEY,
    CodigoCupon VARCHAR(256),
    Descripcion VARCHAR(256),
    Descuento INT
);

CREATE TABLE Tarjetas(
    IDTarjeta INT AUTO_INCREMENT PRIMARY KEY,
    NumeroTarjeta VARCHAR(256),
    Puntos INT,
    IDUsuario INT,
    FOREIGN KEY (IDUsuario) REFERENCES Usuarios (IDUsuario) ON DELETE CASCADE
);

CREATE TABLE Chats(
    IDChat INT AUTO_INCREMENT PRIMARY KEY,
    Emisor INT,
    Receptor INT,
    Mensaje VARCHAR(256),
    FOREIGN KEY (Emisor) REFERENCES Usuarios (IDUsuario) ON DELETE CASCADE,
    FOREIGN KEY (Receptor) REFERENCES Usuarios (IDUsuario) ON DELETE CASCADE
);

CREATE TABLE Medallas(
    IDMedalla INT AUTO_INCREMENT PRIMARY KEY,
    Medalla VARCHAR(256),
    Estado INT,
    IDUsuario INT,
    FOREIGN KEY (IDUsuario) REFERENCES Usuarios (IDUsuario) ON DELETE CASCADE
);

CREATE TABLE Notificaciones(    
    IDNotificacion INT AUTO_INCREMENT PRIMARY KEY,
    Notificacion VARCHAR(256),
    Tipo VARCHAR(255),
    IDUsuario INT,
    FOREIGN KEY (IDUsuario) REFERENCES Usuarios (IDUsuario) ON DELETE CASCADE
);

/*... CARGAR IMAGEN PRODUCTO....*/
DELIMITER //
CREATE PROCEDURE cargar_imagen
(IN id_producto INT, 
	imagen VARCHAR(256)
)
BEGIN
  UPDATE Productos
  SET 
		Imagen = imagen
  WHERE IDProducto = id_producto;
END //
DELIMITER ;

/*... CARGAR IMAGEN USUARIO....*/
DELIMITER //
CREATE PROCEDURE cargar_foto
(IN id_usuario INT, 
	imagen VARCHAR(256)
)
BEGIN
  UPDATE Usuarios
  SET 
		Imagen = imagen
  WHERE IDUsuario = id_usuario;
END //
DELIMITER ;

/*ROLES*/
DELIMITER //
CREATE PROCEDURE sp_GetRoles()
BEGIN
	SELECT * FROM Roles;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostRoles(rol VARCHAR(256))
BEGIN
	INSERT INTO Roles(Rol) VALUES (rol);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutRoles(id_rol INT, rol VARCHAR (100))
BEGIN
	UPDATE Roles SET Rol = rol WHERE IDRol = id_rol;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteRoles(id_rol INT)
BEGIN
	DELETE FROM Roles WHERE IDRol = id_rol;
END //
DELIMITER ;

/*USUARIOS*/
DELIMITER //
CREATE PROCEDURE sp_GetUsuarios()
BEGIN
	SELECT * FROM Usuarios;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostUsuarios(nombre_usuario VARCHAR(256), apellido_usuario VARCHAR(256), direccion_usuario VARCHAR(256),  telefono_usuario VARCHAR(256), correo_usuario VARCHAR(256),  usuario VARCHAR(256),  contrasena VARCHAR(256), estado INT, codigo VARCHAR(256), id_rol INT)
BEGIN
	INSERT INTO Usuarios(NombreUsuario,ApellidoUsuario,DireccionUsuario,TelefonoUsuario,CorreoUsuario,Usuario,Contrasena,Estado,Codigo,IDRol) VALUES (nombre_usuario,apellido_usuario ,direccion_usuario, telefono_usuario,correo_usuario,usuario,contrasena,estado,codigo,id_rol);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutUsuarios(id_usuario INT, nombre_usuario VARCHAR(256), apellido_usuario VARCHAR(256), direccion_usuario VARCHAR(256),  telefono_usuario VARCHAR(256), correo_usuario VARCHAR(256),  contrasena VARCHAR(256), estado INT, codigo VARCHAR(256), id_rol INT)
BEGIN
	UPDATE Usuarios SET NombreUsuario = nombre_usuario, ApellidoUsuario = apellido_usuario ,DireccionUsuario = direccion_usuario, TelefonoUsuario = telefono_usuario, CorreoUsuario = correo_usuario, Contrasena = contrasena, Estado = estado, Codigo =codigo, IDRol = id_rol  WHERE IDUsuario = id_usuario;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutUsuarios2(id_usuario INT, estado INT, contrasena VARCHAR(256), codigo VARCHAR(256) )
BEGIN
	UPDATE Usuarios SET Estado = estado, Contrasena = contrasena, Codigo = codigo WHERE IDUsuario = id_usuario;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteUsuarios(id_usuario INT)
BEGIN
	DELETE FROM Usuarios WHERE IDUsuario = id_usuario;
END //
DELIMITER ;

/*SUCURSALES*/
DELIMITER //
CREATE PROCEDURE sp_GetSucursales()
BEGIN
	SELECT * FROM Sucursales;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostSucursales(sucursal VARCHAR(256),direccion VARCHAR(256), telefono VARCHAR(256), ubicacion VARCHAR(256))
BEGIN
	INSERT INTO Sucursales(Sucursal,Direccion,Telefono,Ubicacion) VALUES (sucursal,direccion,telefono,ubicacion);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutSucursales(id_sucursal INT,sucursal VARCHAR(256),direccion VARCHAR(256), telefono VARCHAR(256), ubicacion VARCHAR(256))
BEGIN
	UPDATE Sucursales SET Sucursal = sucursal, Direccion = direccion, Telefono = telefono, Ubicacion = ubicacion WHERE IDSucursal = id_sucursal;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteSucursales( id_sucursal INT)
BEGIN
	DELETE FROM Sucursales WHERE IDSucursal = id_sucursal;
END //
DELIMITER ;

/*CATEGORIAS*/
DELIMITER //
CREATE PROCEDURE sp_GetCategorias()
BEGIN
	SELECT * FROM Categorias;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostCategorias( categoria VARCHAR(256))
BEGIN
	INSERT INTO Categorias(Categoria) VALUES (categoria);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutCategorias( id_categoria INT,  categoria VARCHAR(256))
BEGIN
	UPDATE Categorias SET Categoria = categoria WHERE IDCategoria = id_categoria;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteCategorias( id_categoria INT)
BEGIN
	DELETE FROM Categorias WHERE IDCategoria = id_categoria;
END //
DELIMITER ;

/*SUBCATEGORIAS*/
DELIMITER //
CREATE PROCEDURE sp_GetSubCategorias()
BEGIN
	SELECT * FROM SubCategorias;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostSubCategorias( subcategoria VARCHAR(256), id_categoria INT)
BEGIN
	INSERT INTO SubCategorias(SubCategoria, IDCategoria) VALUES (subcategoria, id_categoria);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutSubCategorias( id_subcategoria INT,  subcategoria VARCHAR(256), id_categoria INT)
BEGIN
	UPDATE SubCategorias SET SubCategoria = subcategoria, IDCategoria = id_categoria WHERE IDSubCategoria = id_subcategoria;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteSubCategorias( id_subcategoria INT)
BEGIN
	DELETE FROM SubCategorias WHERE IDSubCategoria = id_subcategoria;
END //
DELIMITER ;

/*PROVEEDORES*/
DELIMITER //
CREATE PROCEDURE sp_GetProveedores()
BEGIN
	SELECT * FROM Proveedores;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostProveedores( proveedor VARCHAR(256),direccion VARCHAR(256),telefono VARCHAR(256))
BEGIN
	INSERT INTO Proveedores(Proveedor,Direccion,Telefono) VALUES (proveedor,direccion,telefono);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutProveedores( id_proveedor INT,  proveedor VARCHAR(256),direccion VARCHAR(256),telefono VARCHAR(256))
BEGIN
	UPDATE Proveedores SET Proveedor = proveedor, Direccion = direccion, Telefono = telefono WHERE IDProveedor = id_proveedor;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteProveedores( id_proveedor INT)
BEGIN
	DELETE FROM Proveedores WHERE IDProveedor = id_proveedor;
END //
DELIMITER ;

/*PRODUCTOS*/
DELIMITER //
CREATE PROCEDURE sp_GetProductos()
BEGIN
	SELECT * FROM Productos;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostProductos(  producto VARCHAR(256),  precio DECIMAL(10,2),  cantidad INT,  fecha VARCHAR(256),  estado VARCHAR(256),  id_categoria INT, id_subcategoria INT,  id_proveedor INT,  id_sucursal INT)
BEGIN
	INSERT INTO Productos(Producto,Precio,Cantidad,Fecha,Estado,IDCategoria,IDSubCategoria,IDProveedor,IDSucursal) VALUES (producto,precio,cantidad,fecha,estado,id_categoria,id_subcategoria,id_proveedor,id_sucursal);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutProductos( id_producto INT,  producto VARCHAR(256),  precio DECIMAL(10,2),  cantidad INT,  estado VARCHAR(256),  id_categoria INT,  id_subcategoria INT, id_proveedor INT,  id_sucursal INT)
BEGIN
	UPDATE Productos SET Producto = producto, Precio = precio, Cantidad = cantidad, Estado = estado, IDCategoria = id_categoria, IDSubCategoria = id_subcategoria, IDProveedor = id_proveedor, IDSucursal = id_sucursal WHERE IDProducto = id_producto;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteProductos( id_producto INT)
BEGIN
	DELETE FROM Productos WHERE IDProducto = id_producto;
END //
DELIMITER ;

/*COMENTARIOS*/
DELIMITER //
CREATE PROCEDURE sp_GetComentarios()
BEGIN
	SELECT * FROM Comentarios;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostComentarios(comentario VARCHAR(256), estrellas INT, id_producto INT, id_usuario INT)
BEGIN
	INSERT INTO Comentarios(Comentario,Estrellas,IDProducto,IDUsuario) VALUES (comentario,estrellas,id_producto,id_usuario);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutComentarios( id_comentario INT, comentario VARCHAR(256), estrellas INT, id_producto INT, id_usuario INT)
BEGIN
	UPDATE Comentarios SET Comentario = comentario, Estrellas = estrellas, IDProducto = id_producto, IDUsuario = id_usuario WHERE IDComentario = id_comentario;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteComentarios( id_comentario INT)
BEGIN
	DELETE FROM Comentarios WHERE IDComentario = id_comentario;
END //
DELIMITER ;

/*FACTURAS*/
DELIMITER //
CREATE PROCEDURE sp_GetFacturas()
BEGIN
	SELECT * FROM Facturas;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostFacturas( codigo_factura VARCHAR(256), total DECIMAL(10,2), id_pedido INT)
BEGIN
	INSERT INTO Facturas(CodigoFactura,Total,IDPedido) VALUES (codigo_factura,total,id_pedido);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutFacturas(id_factura INT,  codigo_factura VARCHAR(256),  total DECIMAL(10,2), id_pedido INT)
BEGIN
	UPDATE Facturas SET CodigoFactura = codigo_factura, Total = total, IDPedido = id_pedido WHERE IDFactura = id_factura;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteFacturas( id_factura INT)
BEGIN
	DELETE FROM Facturas WHERE IDFactura = id_factura;
END //
DELIMITER ;

/*DETALLE FACTURAS*/
DELIMITER //
CREATE PROCEDURE sp_GetDetalleFacturas()
BEGIN
	SELECT * FROM DetalleFacturas;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostDetalleFacturas( id_producto INT,  cantidad INT,  subtotal DECIMAL(10,2), id_factura INT)
BEGIN
	INSERT INTO DetalleFacturas(IDProducto,Cantidad,SubTotal,IDFactura) VALUES (id_producto,cantidad,subtotal,id_factura);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutDetalleFacturas(id_detalle_factura INT,  id_producto INT,  cantidad INT,  subtotal DECIMAL(10,2), id_factura INT)
BEGIN
	UPDATE DetalleFacturas SET IDProducto = id_producto, Cantidad = cantidad, SubTotal = subtotal WHERE IDDetalleFactura = id_detalle_factura;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteDetalleFacturas( id_detalle_factura INT)
BEGIN
	DELETE FROM DetalleFacturas WHERE IDDetalleFactura = id_detalle_factura;
END //
DELIMITER ;

/*CUPONES*/
DELIMITER //
CREATE PROCEDURE sp_GetCupones()
BEGIN
	SELECT * FROM Cupones;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostCupones( codigo_cupon VARCHAR(256),  descripcion VARCHAR(256),  descuento INT)
BEGIN
	INSERT INTO Cupones(CodigoCupon,Descripcion,Descuento) VALUES (codigo_cupon,descripcion,descuento);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutCupones( id_cupon INT,  codigo_cupon VARCHAR(256),  descripcion VARCHAR(256),  descuento INT)
BEGIN
	UPDATE Cupones SET CodigoCupon = codigo_cupon, Descripcion = descripcion, Descuento = descuento WHERE IDCupon = id_cupon;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteCupones( id_cupon INT)
BEGIN
	DELETE FROM Cupones WHERE IDCupon = id_cupon;
END //
DELIMITER ;

/*TARJETAS*/
DELIMITER //
CREATE PROCEDURE sp_GetTarjetas()
BEGIN
	SELECT * FROM Tarjetas;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostTarjetas( numero_tarjeta VARCHAR(256),  puntos INT, id_usuario INT)
BEGIN
	INSERT INTO Tarjetas(NumeroTarjeta,Puntos,IDUsuario) VALUES (numero_tarjeta,puntos,id_usuario);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutTarjetas( id_tarjeta INT,  numero_tarjeta VARCHAR(256),  puntos INT, id_usuario INT)
BEGIN
	UPDATE Tarjetas SET NumeroTarjeta = numero_tarjeta, Puntos = puntos, IDUsuario = id_usuario WHERE IDTarjeta = id_tarjeta;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteTarjetas( id_tarjeta INT)
BEGIN
	DELETE FROM Tarjetas WHERE IDTarjeta = id_tarjeta;
END //
DELIMITER ;

/*CHATS*/
DELIMITER //
CREATE PROCEDURE sp_GetChats()
BEGIN
	SELECT * FROM Chats;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostChats( emisor INT,  receptor INT,  mensaje VARCHAR(256))
BEGIN
	INSERT INTO Chats(Emisor,Receptor,Mensaje) VALUES (emisor,receptor,mensaje);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutChats( id_chat INT , emisor INT,  receptor INT,  mensaje VARCHAR(256))
BEGIN
	UPDATE Chats SET Emisor = emisor, Receptor = receptor, Mensaje = mensaje WHERE IDChat = id_chat;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteChats( id_chat INT)
BEGIN
	DELETE FROM Chats WHERE IDChat = id_chat;
END //
DELIMITER ;

/*ESTADOS*/
DELIMITER //
CREATE PROCEDURE sp_GetEstados()
BEGIN
	SELECT * FROM Estados;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostEstados( estado VARCHAR(256),  porcentaje INT)
BEGIN
	INSERT INTO Estados(Estado,Porcentaje) VALUES (estado,porcentaje);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutEstados( id_estado INT , estado VARCHAR(256),  porcentaje INT)
BEGIN
	UPDATE Estados SET Estado = estado, Porcentaje = porcentaje WHERE IDEstado = id_estado;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteEstados( id_estado INT)
BEGIN
	DELETE FROM Estados WHERE IDEstado = id_estado;
END //
DELIMITER ;

/*PEDIDOS*/
DELIMITER //
CREATE PROCEDURE sp_GetPedidos()
BEGIN
	SELECT * FROM Pedidos;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostPedidos( codigo_pedido VARCHAR(256), id_estado INT, id_usuario INT )
BEGIN
	INSERT INTO Pedidos(CodigoPedido,IDEstado,IDUsuario) VALUES (codigo_pedido,id_estado,id_usuario);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutPedidos( id_pedido INT, codigo_pedido VARCHAR(256), id_estado INT, id_usuario INT )
BEGIN
	UPDATE Pedidos SET CodigoPedido = codigo_pedido, IDEstado = id_estado, IDUsuario = id_usuario WHERE IDPedido = id_pedido;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeletePedidos( id_pedido INT)
BEGIN
	DELETE FROM Pedidos WHERE IDPedido = id_pedido;
END //
DELIMITER ;

/*MEDALLAS*/
DELIMITER //
CREATE PROCEDURE sp_GetMedallas()
BEGIN
	SELECT * FROM Medallas;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostMedallas( medalla VARCHAR(256), estado INT, id_usuario INT )
BEGIN
	INSERT INTO Medallas(Medalla,Estado,IDUsuario) VALUES (medalla,estado,id_usuario);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutMedallas( id_medalla INT, medalla VARCHAR(256), estado INT, id_usuario INT )
BEGIN
	UPDATE Medallas SET Medalla = medalla, Estado = estado, IDUsuario = id_usuario WHERE IDMedalla = id_medalla;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteMedallas( id_medalla INT)
BEGIN
	DELETE FROM Medallas WHERE IDMedalla = id_medalla;
END //
DELIMITER ;

/*NOTIFICACIONES*/
DELIMITER //
CREATE PROCEDURE sp_GetNotificaciones()
BEGIN
	SELECT * FROM Notificaciones;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PostNotificaciones( notificacion VARCHAR(256), tipo VARCHAR(256), id_usuario INT )
BEGIN
	INSERT INTO Notificaciones(Notificacion,Tipo,IDUsuario) VALUES (notificacion,tipo,id_usuario);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_PutNotificaciones( id_notificacion INT, notificacion VARCHAR(256), tipo VARCHAR(256), id_usuario INT )
BEGIN
	UPDATE Notificaciones SET Notificacion = notificacion, Tipo = tipo, IDUsuario = id_usuario WHERE IDNotificacion = id_notificacion;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sp_DeleteNotificaciones( id_notificacion INT)
BEGIN
	DELETE FROM Notificaciones WHERE IDNotificacion = id_notificacion;
END //
DELIMITER ;

CALL sp_PostRoles("Administrador");
CALL sp_PostRoles("Cajero");
CALL sp_PostRoles("Usuario");
CALL sp_PostUsuarios("Administrador", "Administrador", "5av 11-54 Zona 7 Landivar", "46480583", "admin@gmail.com", "admin", "admin", 1, "", 1);
CALL sp_PostUsuarios("Cajero", "Cajero", "5av 11-54 Zona 7 Landivar", "46480583", "cajero@cajero.com", "cajero", "cajero", 1, "", 2);
CALL sp_PostUsuarios("Cliente", "Cliente", "5av 11-54 Zona 7 Landivar", "46480583", "cliente@cliente.com", "cliente", "cliente", 1, "",3);
CALL sp_PostEstados("Anulado",0);
CALL sp_PostEstados("Cancelado",0);
CALL sp_PostEstados("Procesando",20);
CALL sp_PostEstados("Pendiente",40);
CALL sp_PostEstados("Empacando",60);
CALL sp_PostEstados("Enviando",80);
CALL sp_PostEstados("Completado",100);
CALL sp_PostCategorias("Ropa");
CALL sp_PostCategorias("Comida");
CALL sp_PostCategorias("Entretenimiento");
CALL sp_PostCategorias("Electronicos");
CALL sp_PostSubCategorias("Camisas", 1);
CALL sp_PostSubCategorias("Pantalones", 1);
CALL sp_PostSubCategorias("Frutas", 2);
CALL sp_PostSubCategorias("Lacteos", 2);
CALL sp_PostSubCategorias("Juguetes", 3);
CALL sp_PostSubCategorias("Consolas", 3);
CALL sp_PostSubCategorias("Telefonos", 4);
CALL sp_PostSubCategorias("Computadoras", 4);
CALL sp_PostSucursales("Miraflores", "Zona 11", "4354 - 5435", "Maps");
CALL sp_PostSucursales("NaranjoMall", "Mixco", "4354 - 5435", "Maps");
CALL sp_PostSucursales("OaklandMall", "Zona 10", "4354 - 5435", "Maps");
CALL sp_PostSucursales("Portales", "Zona 17", "4354 - 5435", "Maps");
CALL sp_PostSucursales("Cayala", "Zona 16", "4354 - 5435", "Maps");
CALL sp_PostSucursales("ElFrutal", "Villa Nueva", "4354 - 5435", "Maps");
CALL sp_PostSucursales("SankrisMall", "San Cristobal", "4354 - 5435", "Maps");
CALL sp_PostSucursales("SantaClara", "Villa Nueva", "4354 - 5435", "Maps");
CALL sp_PostProveedores("La Chiquita", "Zona 11", "4556 - 6334");
CALL sp_PostProveedores("Zara", "Zona 8", "4556 - 6334");
CALL sp_PostProveedores("Siman", "Zona 9", "4556 - 6334");
CALL sp_PostProveedores("Perry", "Zona 3", "4556 - 6334");
CALL sp_PostProveedores("Lala", "Zona 2", "4556 - 6334");
CALL sp_PostProveedores("Toys", "Zona 9", "4556 - 6334");
CALL sp_PostProveedores("Wii", "Zona 1", "4556 - 6334");
CALL sp_PostProveedores("Samsung", "Zona 3", "4556 - 6334");
CALL sp_PostProveedores("Apple", "Zona 4", "4556 - 6334");
CALL sp_PostProductos("Manzana","3","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Pera","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Sandia","15","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Melon","10","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Papaya","15","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Durazno","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Kiwi","5","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Zapote","5","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Fresas","6","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Mango","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Aguacate","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Arandanos","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Banano","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Cereza","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Ciruela","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Coco","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Granada","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Lichas","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Limon","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Longan","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Naranja","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Piña","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Tomate","4","10","05/06/2016","Disponible",2,1,1,1);
CALL sp_PostProductos("Uvas","4","10","05/06/2016","Disponible",2,1,1,1);