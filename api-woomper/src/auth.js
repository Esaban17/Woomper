const Usuario = require('./models/UsuariosModel');
const jwt = require('jsonwebtoken');
var auth = {};
var e;

auth.Verificar = function(req, res) {
    var usuarioToken = {};
    try {
        usuarioToken = jwt.verify(req.headers.authorization, "Contrasena");
    } catch (error) {
        return true;
    }
    Usuario.login(usuarioToken.Usuario, usuarioToken.Contrasena, (err, data) => {
        if(!(data === null)) {
            return false;
        }else{
            return true;
        }
    });
}

module.exports = auth