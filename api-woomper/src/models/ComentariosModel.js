const connection = require('../db/Connection');

let comentariosModel = {};

comentariosModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getComentarios()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

comentariosModel.post = (comentariosData, callback) => {
    if(connection){
        const sql = `CALL sp_PostComentarios("${comentariosData.Comentario}","${comentariosData.Estrellas}","${comentariosData.IDProducto}","${comentariosData.IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Insertado"});
            }
        })
    }
};

comentariosModel.put = (comentariosData, callback) => {
    if(connection) {
        const sql = `CALL sp_PutComentarios("${comentariosData.IDComentario}","${comentariosData.Comentario}","${comentariosData.Estrellas}","${comentariosData.IDProducto}","${comentariosData.IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Editado"});
            }
        })
    }
};

comentariosModel.delete = (IDComentario, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteComentarios("${IDComentario}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = comentariosModel;