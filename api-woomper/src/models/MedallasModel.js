const connection = require('../db/Connection');

let medallasModel = {};

medallasModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getMedallas()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

medallasModel.post = (medallasData, callback) => {
    if(connection){
        const sql = `CALL sp_PostMedallas("${medallasData.Medalla}","${medallasData.Estado}","${medallasData.IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

medallasModel.put = (medallasData, callback) => {
    if(connection){
        const sql = `CALL sp_PutMedallas("${medallasData.IDMedalla}","${medallasData.Medalla}","${medallasData.Estado}","${medallasData.IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

medallasModel.delete = (IDMedalla, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteMedallas("${IDMedalla}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = medallasModel;