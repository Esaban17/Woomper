const connection = require('../db/Connection');

let pedidosModel = {};

pedidosModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getPedidos()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

pedidosModel.post = (pedidosData, callback) => {
    if(connection){
        const sql = `CALL sp_PostPedidos("${pedidosData.CodigoPedido}","${pedidosData.IDEstado}","${pedidosData.IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

pedidosModel.put = (pedidosData, callback) => {
    if(connection){
        const sql = `CALL sp_PutPedidos("${pedidosData.IDPedido}","${pedidosData.CodigoPedido}","${pedidosData.IDEstado}","${pedidosData.IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

pedidosModel.delete = (IDPedido, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeletePedidos("${IDPedido}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = pedidosModel;