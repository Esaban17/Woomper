const connection = require('../db/Connection');

let usuarioModel = {};

usuarioModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getUsuarios()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

usuarioModel.get2 = (callback) => {
    if(connection){
        const sql = `SELECT IDUsuario, NombreUsuario FROM Usuarios WHERE 1`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows);
            }
        })
    }
};

usuarioModel.get3 = (callback) => {
    if(connection){
        const sql = `SELECT IDUsuario, NombreUsuario, ApellidoUsuario, CorreoUsuario, Usuario, Codigo, Estado, Contrasena, Imagen FROM Usuarios WHERE 1`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows);
            }
        })
    }
};

usuarioModel.insertImagen = function(usuarioData, callback) {
    if(connection) {
        const sql = 'CALL cargar_foto(?, ?)';
            connection.query(sql, [usuarioData.IDUsuario, usuarioData.Imagen],(err, res) =>{
              if(err) {
                  throw err;
              }else {
                callback(null, {
                    "msg": "success"
                });
              }
          });
      }
};

usuarioModel.post = (usuarioData, callback) => {
    if(connection){
        const sql = `CALL sp_PostUsuarios("${usuarioData.NombreUsuario}","${usuarioData.ApellidoUsuario}","${usuarioData.DireccionUsuario}","${usuarioData.TelefonoUsuario}","${usuarioData.CorreoUsuario}","${usuarioData.Usuario}","${usuarioData.Contrasena}","${usuarioData.Estado}","${usuarioData.Codigo}","${usuarioData.IDRol}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

usuarioModel.put = (usuarioData, callback) => {
    if(connection){
        const sql = `CALL sp_PutUsuarios("${usuarioData.IDUsuario}","${usuarioData.NombreUsuario}","${usuarioData.ApellidoUsuario}","${usuarioData.DireccionUsuario}","${usuarioData.TelefonoUsuario}","${usuarioData.CorreoUsuario}","${usuarioData.Contrasena}","${usuarioData.Estado}","${usuarioData.Codigo}","${usuarioData.IDRol}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

usuarioModel.put2 = (usuarioData, callback) => {
    if(connection){
        const sql = `CALL sp_PutUsuarios2("${usuarioData.IDUsuario}","${usuarioData.Estado}","${usuarioData.Contrasena}","${usuarioData.Codigo}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

usuarioModel.delete = (IDUsuario, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteUsuarios("${IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

usuarioModel.login2 = (Usuario, Codigo, callback) => {
    if(connection){
        connection.query("SELECT * FROM Usuarios WHERE Usuario = ? AND Codigo = ?",
        [Usuario, Codigo],
        (err, rows) => {
            if(err){
                throw err;
            }else{
                if (rows.length > 0) {
                    callback(null, rows[0]);
                } else {
                    callback("No Encontrado", null);
                }
            }
        });
    }
};

usuarioModel.login = (Usuario, Contrasena, callback) => {
    if(connection){
        connection.query("SELECT * FROM Usuarios WHERE Usuario = ? AND Contrasena = ?",
        [Usuario, Contrasena],
        (err, rows) => {
            if(err){
                throw err;
            }else{
                if (rows.length > 0) {
                    callback(null, rows[0]);
                } else {
                    callback("No Encontrado", null);
                }
            }
        });
    }
};

usuarioModel.register = (Usuario, callback) => {
    if(connection){
        connection.query("SELECT * FROM Usuarios WHERE Usuario = ?",
        [Usuario],
        (err, rows) => {
            if(err){
                throw err;
            }else{
                if (rows.length > 0) {
                    callback(null, rows[0]);
                } else {
                    callback("Registro Exitoso", null);
                }
            }
        });
    }
};

module.exports = usuarioModel;