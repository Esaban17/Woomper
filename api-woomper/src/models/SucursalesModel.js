const connection = require('../db/Connection');

let sucursalesModel = {};

sucursalesModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getSucursales()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

sucursalesModel.post = (sucursalesData, callback) => {
    if(connection){
        const sql = `CALL sp_PostSucursales("${sucursalesData.Sucursal}","${sucursalesData.Direccion}","${sucursalesData.Telefono}","${sucursalesData.Ubicacion}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

sucursalesModel.put = (sucursalesData, callback) => {
    if(connection){
        const sql = `CALL sp_PutSucursales("${sucursalesData.IDSucursal}","${sucursalesData.Sucursal}","${sucursalesData.Direccion}","${sucursalesData.Telefono}","${sucursalesData.Ubicacion}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

sucursalesModel.delete = (IDSucursal, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteSucursales("${IDSucursal}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = sucursalesModel;