const connection = require('../db/Connection');

let rolesModel = {};

rolesModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getRoles()`;

        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

module.exports = rolesModel;