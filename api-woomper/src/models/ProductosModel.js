const connection = require('../db/Connection');

let productosModel = {};

productosModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getProductos()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

productosModel.insertImagen = function(productosData, callback) {
    if(connection) {
        const sql = 'CALL cargar_imagen(?, ?)';
            connection.query(sql, [productosData.IDProducto, productosData.Imagen],(err, res) =>{
              if(err) {
                  throw err;
              }else {
                callback(null, {
                    "msg": "success"
                });
              }
          });
      }
};

productosModel.post = (productosData, callback) => {
    if(connection){
        const sql = `CALL sp_PostProductos("${productosData.Producto}","${productosData.Precio}","${productosData.Cantidad}","${productosData.Fecha}","${productosData.Estado}","${productosData.IDCategoria}","${productosData.IDSubCategoria}","${productosData.IDProveedor}","${productosData.IDSucursal}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

productosModel.put = (productosData, callback) => {
    if(connection){
        const sql = `CALL sp_PutProductos("${productosData.IDProducto}","${productosData.Producto}","${productosData.Precio}","${productosData.Cantidad}","${productosData.Estado}","${productosData.IDCategoria}","${productosData.IDSubCategoria}","${productosData.IDProveedor}","${productosData.IDSucursal}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

productosModel.delete = (IDProducto, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteProductos("${IDProducto}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = productosModel;