const connection = require('../db/Connection');

let proveedoresModel = {};

proveedoresModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getProveedores()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

proveedoresModel.post = (proveedoresData, callback) => {
    if(connection){
        const sql = `CALL sp_PostProveedores("${proveedoresData.Proveedor}","${proveedoresData.Direccion}","${proveedoresData.Telefono}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

proveedoresModel.put = (proveedoresData, callback) => {
    if(connection){
        const sql = `CALL sp_PutProveedores("${proveedoresData.IDProveedor}","${proveedoresData.Proveedor}","${proveedoresData.Direccion}","${proveedoresData.Telefono}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

proveedoresModel.delete = (IDProveedor, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteProveedores("${IDProveedor}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = proveedoresModel;