const connection = require('../db/Connection');

let cuponesModel = {};

cuponesModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getCupones()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

cuponesModel.post = (cuponesData, callback) => {
    if(connection){
        const sql = `CALL sp_PostCupones("${cuponesData.CodigoCupon}","${cuponesData.Descripcion}","${cuponesData.Descuento}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

cuponesModel.put = (cuponesData, callback) => {
    if(connection) {
        const sql = `CALL sp_PutCupones("${cuponesData.IDCupon}","${cuponesData.CodigoCupon}","${cuponesData.Descripcion}","${cuponesData.Descuento}")`;
    
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

cuponesModel.delete = (IDCupon, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteCupones("${IDCupon}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = cuponesModel;