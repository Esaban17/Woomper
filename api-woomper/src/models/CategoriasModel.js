const connection = require('../db/Connection');

let categoriasModel = {};

categoriasModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getCategorias()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

categoriasModel.post = (categoriasData, callback) => {
    if(connection){
        const sql = `CALL sp_PostCategorias("${categoriasData.Categoria}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

categoriasModel.put = (categoriasData, callback) => {
    if(connection) {
        const sql = `CALL sp_PutCategorias("${categoriasData.IDCategoria}","${categoriasData.Categoria}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

categoriasModel.delete = (IDCategoria, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteCategorias("${IDCategoria}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = categoriasModel;