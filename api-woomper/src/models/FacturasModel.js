const connection = require('../db/Connection');

let facturasModel = {};

facturasModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getFacturas()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

facturasModel.post = (facturasData, callback) => {
    if(connection){
        const sql = `CALL sp_PostFacturas("${facturasData.CodigoFactura}","${facturasData.Total}","${facturasData.IDPedido}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

facturasModel.put = (facturasData, callback) => {
    if(connection){
        const sql = `CALL sp_PutFacturas("${facturasData.IDFactura}","${facturasData.CodigoFactura}","${facturasData.Total}","${facturasData.IDPedido}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

facturasModel.delete = (IDFactura, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteFacturas("${IDFactura}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = facturasModel;