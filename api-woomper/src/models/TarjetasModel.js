const connection = require('../db/Connection');

let tarjetasModel = {};

tarjetasModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getTarjetas()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

tarjetasModel.post = (tarjetasData, callback) => {
    if(connection){
        const sql = `CALL sp_PostTarjetas("${tarjetasData.NumeroTarjeta}","${tarjetasData.Puntos}","${tarjetasData.IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

tarjetasModel.put = (tarjetasData, callback) => {
    if(connection){
        const sql = `CALL sp_PutTarjetas("${tarjetasData.IDTarjeta}","${tarjetasData.NumeroTarjeta}","${tarjetasData.Puntos}","${tarjetasData.IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

tarjetasModel.delete = (IDTarjeta, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteTarjetas("${IDTarjeta}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = tarjetasModel;