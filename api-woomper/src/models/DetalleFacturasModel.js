const connection = require('../db/Connection');

let detallefacturasModel = {};

detallefacturasModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getDetalleFacturas()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

detallefacturasModel.post = (detallefacturasData, callback) => {
    if(connection){
        const sql = `CALL sp_PostDetalleFacturas("${detallefacturasData.IDProducto}","${detallefacturasData.Cantidad}","${detallefacturasData.SubTotal}","${detallefacturasData.IDFactura}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

detallefacturasModel.put = (detallefacturasData, callback) => {
    if(connection){
        const sql = `CALL sp_PutDetalleFacturas("${detallefacturasData.IDDetalleFactura}","${detallefacturasData.IDProducto}","${detallefacturasData.Cantidad}","${detallefacturasData.SubTotal}","${categoriasData.IDFactura}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

detallefacturasModel.delete = (IDDetalleFactura, callback) => {
    if(connection){
        const sql = `CALL sp_DeleteDetalleFacturas("${IDDetalleFactura}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Eliminado"});
            }
        })
    }
};

module.exports = detallefacturasModel;