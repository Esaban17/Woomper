const connection = require('../db/Connection');

let notificacionesModel = {};

notificacionesModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getNotificaciones()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

notificacionesModel.post = (notificacionesData, callback) => {
    if(connection){
        const sql = `CALL sp_PostNotificaciones("${notificacionesData.Notificacion}","${notificacionesData.Tipo}","${notificacionesData.IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

notificacionesModel.put = (notificacionesData, callback) => {
    if(connection){
        const sql = `CALL sp_PutNotificaciones("${notificacionesData.IDNotificacion}","${notificacionesData.Notificacion}","${notificacionesData.Tipo}","${notificacionesData.IDUsuario}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

notificacionesModel.delete = (IDNotificacion, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteNotificaciones("${IDNotificacion}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = notificacionesModel;