const connection = require('../db/Connection');

let chatsModel = {};

chatsModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getChats()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

chatsModel.post = (chatsData, callback) => {
    if(connection){
        const sql = `CALL sp_PostChats("${chatsData.Emisor}","${chatsData.Receptor}","${chatsData.Mensaje}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

chatsModel.put = (chatsData, callback) => {
    if(connection) {
        const sql = `CALL sp_PutChats("${chatsData.IDChat}","${chatsData.Emisor}","${chatsData.Receptor}","${chatsData.Mensaje}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

chatsModel.delete = (IDChat, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteChats("${IDChat}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = chatsModel; 