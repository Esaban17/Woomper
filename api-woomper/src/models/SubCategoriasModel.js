const connection = require('../db/Connection');

let subcategoriasModel = {};

subcategoriasModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getSubCategorias()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

subcategoriasModel.post = (subcategoriasData, callback) => {
    if(connection){
        const sql = `CALL sp_PostSubCategorias("${subcategoriasData.SubCategoria}","${subcategoriasData.IDCategoria}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Insertado"});
            }
        })
    }
};

subcategoriasModel.put = (subcategoriasData, callback) => {
    if(connection){
        const sql = `CALL sp_PutSubCategorias("${subcategoriasData.IDSubCategoria}","${subcategoriasData.SubCategoria}","${subcategoriasData.IDCategoria}")`;
        connection.query(sql, (err, res) => {
            if(err){
                throw err;
            }else{
                callback(null, {"msg": "Registro Editado"});
            }
        })
    }
};

subcategoriasModel.delete = (IDSubCategoria, callback) =>{
    if(connection) {
        const sql = `CALL sp_DeleteSubCategorias("${IDSubCategoria}")`;
        connection.query(sql, (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {"msg": "Registro Eliminado"});
            }
        });
    }
};

module.exports = subcategoriasModel;