const connection = require('../db/Connection');

let estadosModel = {};

estadosModel.get = (callback) => {
    if(connection){
        const sql = `CALL sp_getEstados()`;
        connection.query(sql, (err, rows) => {
            if(err){
                throw err;
            }else{
                callback(null, rows[0]);
            }
        })
    }
};

module.exports = estadosModel;