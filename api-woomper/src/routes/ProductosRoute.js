const Productos = require('../models/ProductosModel');
const fse = require('fs-extra');
const formidable = require('formidable');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/productos', (req, res) => {
        Productos.get((err, data) => {
            res.status(200).json(data);
        });
    });

    app.get('/productos/imagen/:Imagen', (req, res) => {
        var parametro = req.params.Imagen;
        var ruta = 'src/images/productos/';
        fse.readFile(ruta + parametro, (err, imagen) => {
            if(err){
                res.json({
                    message: 'Imagen No Encontrada'
                });
            }else{
                res.send(imagen);
            }
        })
    });

    app.post('/productos', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const productosData = {
                Producto: req.body.Producto,
                Precio: req.body.Precio,
                Cantidad: req.body.Cantidad,
                Fecha: req.body.Fecha,
                Estado: req.body.Estado,
                IDCategoria: req.body.IDCategoria,
                IDSubCategoria: req.body.IDSubCategoria,
                IDProveedor: req.body.IDProveedor,
                IDSucursal: req.body.IDSucursal
            }
            Productos.post(productosData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.post('/upload/producto', (req, res, next) => {
        //Parseamos la Imagen
        var form = new formidable.IncomingForm();
        form.keepExtensions = true;
        form.maxFieldsSize = 10 * 1024 * 1024; //10 MB
        form.multiples = true;
        form.parse(req, (err, fields, files) => {
            if (err) {
                res.json({
                    result: "failed",
                    data: {},
                    messege: `No se pudo subir la imagen.Error is : ${err}`
                });
            }
            console.log(files)
            var arrayOfFiles = [];
            if(files[""] instanceof Array) {
                arrayOfFiles = files[""];
            } else {
                arrayOfFiles.push(files[""]);
            }         
            if (arrayOfFiles.length > 0) {
                //Cambiamos de ruta la imagen
                fse.copy(files.Imagen.path, 'src/images/productos/'+ files.Imagen.name, err => {
                    if (err) return console.error(err)
                    console.log('Exito al Subir la Imagen!')
                });
                const productosData = {
                    IDProducto: fields.IDProducto,
                    Imagen: files.Imagen.name
                };
                Productos.insertImagen(productosData, (err, data) => {
                    console.log(data)
                    if(data && data.msg) {
                        console.log('Imagen Insertada');
                    }else {
                        console.log('Error Al Insertar la Imagen')
                    }
                });
                res.json({
                    result: "Exito",
                    data: files.name,
                    messege: "Imagen Subida Correctamente",
                    files,
                    fields
                });
            } else {
                res.json({
                    result: "Error",
                    data: {},
                    numberOfImages: 0,
                    messege: "Imagen no Subida!"
                });
            }
        });
    });
    
    app.put('/productos/:IDProducto', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const productosData = {
                IDProducto: req.params.IDProducto,
                Producto: req.body.Producto,
                Precio: req.body.Precio,
                Cantidad: req.body.Cantidad,
                Estado: req.body.Estado,
                IDCategoria: req.body.IDCategoria,
                IDSubCategoria: req.body.IDSubCategoria,
                IDProveedor: req.body.IDProveedor,
                IDSucursal: req.body.IDSucursal
            };
            Productos.put(productosData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/productos/:IDProducto', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Productos.delete(req.params.IDProducto, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

}