const Sucursales = require('../models/SucursalesModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/sucursales', (req, res) => {
        Sucursales.get((err, data) => {
            res.status(200).json(data);
        });
    });

    app.post('/sucursales', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const sucursalesData = {
                Sucursal: req.body.Sucursal,
                Direccion: req.body.Direccion,
                Telefono: req.body.Telefono,
                Ubicacion: req.body.Ubicacion
            }
            Sucursales.post(sucursalesData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/sucursales/:IDSucursal', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const sucursalesData = {
                IDSucursal: req.params.IDSucursal,
                Sucursal: req.body.Sucursal,
                Direccion: req.body.Direccion,
                Telefono: req.body.Telefono,
                Ubicacion: req.body.Ubicacion
            };
            Sucursales.put(sucursalesData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/sucursales/:IDSucursal', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Sucursales.delete(req.params.IDSucursal, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });
}