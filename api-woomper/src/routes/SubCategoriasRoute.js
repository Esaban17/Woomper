const SubCategorias = require('../models/SubCategoriasModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/subcategorias', (req, res) => {
        SubCategorias.get((err, data) => {
            res.status(200).json(data);
        });
    });

    app.post('/subcategorias', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const subcategoriasData = {
                SubCategoria: req.body.SubCategoria,
                IDCategoria: req.body.IDCategoria
            }
            SubCategorias.post(subcategoriasData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/subcategorias/:IDSubCategoria', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const subcategoriasData = {
                IDSubCategoria: req.params.IDSubCategoria,
                SubCategoria: req.body.SubCategoria,
                IDCategoria: req.body.IDCategoria
            };
            SubCategorias.put(subcategoriasData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/subcategorias/:IDSubCategoria', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            SubCategorias.delete(req.params.IDSubCategoria, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

}