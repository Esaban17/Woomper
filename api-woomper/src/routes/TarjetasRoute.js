const Tarjetas = require('../models/TarjetasModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/tarjetas', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Tarjetas.get((err, data) => {
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.post('/tarjetas', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const tarjetasData = {
                NumeroTarjeta: req.body.NumeroTarjeta,
                Puntos: req.body.Puntos,
                IDUsuario: req.body.IDUsuario
            }
            Tarjetas.post(tarjetasData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/tarjetas/:IDTarjeta', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const tarjetasData = {
                IDTarjeta: req.params.IDTarjeta,
                NumeroTarjeta: req.body.NumeroTarjeta,
                Puntos: req.body.Puntos,
                IDUsuario: req.body.IDUsuario
            };
            Tarjetas.put(tarjetasData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/tarjetas/:IDTarjeta', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Tarjetas.delete(req.params.IDTarjeta, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });
}