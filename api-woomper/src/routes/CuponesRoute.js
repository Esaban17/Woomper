const Cupones = require('../models/CuponesModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/cupones', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Cupones.get((err, data) => {
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.post('/cupones', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const cuponesData = {
                CodigoCupon: req.body.CodigoCupon,
                Descripcion: req.body.Descripcion,
                Descuento: req.body.Descuento
            }
            Cupones.post(cuponesData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/cupones/:IDCupon', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const cuponesData = {
                IDCupon: req.params.IDCupon,
                CodigoCupon: req.body.CodigoCupon,
                Descripcion: req.body.Descripcion,
                Descuento: req.body.Descuento
            };
            Cupones.put(cuponesData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/cupones/:IDCupon', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Cupones.delete(req.params.IDCupon, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

}