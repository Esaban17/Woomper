const Proveedores = require('../models/ProveedoresModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/proveedores', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Proveedores.get((err, data) => {
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.post('/proveedores', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const proveedoresData = {
                Proveedor: req.body.Proveedor,
                Direccion: req.body.Direccion,
                Telefono: req.body.Telefono
            }
            Proveedores.post(proveedoresData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/proveedores/:IDProveedor', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const proveedoresData = {
                IDProveedor: req.params.IDProveedor,
                Proveedor: req.body.Proveedor,
                Direccion: req.body.Direccion,
                Telefono: req.body.Telefono
            };
            Proveedores.put(proveedoresData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/proveedores/:IDProveedor', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Proveedores.delete(req.params.IDProveedor, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

}