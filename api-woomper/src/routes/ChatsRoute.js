const Chats = require('../models/ChatsModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/chats', (req, res) => {
        Chats.get((err, data) => {
            res.status(200).json(data);
        });
    });

    app.post('/chats', (req, res) => {
        const chatsData = {
            Emisor: req.body.Emisor,
            Receptor: req.body.Receptor,
            Mensaje: req.body.Mensaje
        }
        Chats.post(chatsData, (err, data) => {
            if(data && data.msg) {
                res.json(data);
            }else{
                res.json({msg: "Error Al Insertar"})
            }
        });
    });

    app.put('/chats/:IDChat', (req, res) => {
        const chatsData = {
            IDChat: req.params.IDChat,
            Emisor: req.body.Emisor,
            Receptor: req.body.Receptor,
            Mensaje: req.body.Mensaje
        };
        Chats.put(chatsData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    msg: "Error Al Editar"
                })
            }
        });
    });
    
    app.delete('/chats/:IDChat', (req, res) => {
        Chats.delete(req.params.IDChat, (err, data) => {
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    msg: "Error Al Eliminar"
                })
            }
        });
    });

}