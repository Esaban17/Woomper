const Usuario = require('../models/UsuariosModel');
const fse = require('fs-extra');
const formidable = require('formidable');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app) {

    app.get('/usuarios', (req, res) => {
        e = auth.Verificar(req);   
        if(e == undefined) {
            Usuario.get((err, data) =>{
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.get('/cantusuarios', (req, res) => {
        Usuario.get2((err, data) =>{
            res.status(200).json(data);
        });
    });

    app.get('/usuarios2', (req, res) => {
        Usuario.get3((err, data) =>{
            res.status(200).json(data);
        });
    });
    
    app.get('/usuarios/imagen/:Imagen', (req, res) => {
        var parametro = req.params.Imagen;
        var ruta = 'src/images/usuarios/';
        fse.readFile(ruta + parametro, (err, imagen) => {
            if(err){
                res.json({
                    message: 'Imagen No Encontrada'
                });
            }else{
                res.send(imagen);
            }
        })
    });

    app.post('/usuarios', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const usuarioData = {
                NombreUsuario: req.body.NombreUsuario,
                ApellidoUsuario: req.body.ApellidoUsuario,
                DireccionUsuario: req.body.DireccionUsuario,
                TelefonoUsuario: req.body.TelefonoUsuario,
                CorreoUsuario: req.body.CorreoUsuario,                    
                Usuario: req.body.Usuario,
                Contrasena: req.body.Contrasena,
                Estado: req.body.Estado,
                Codigo: req.body.Codigo,
                IDRol: req.body.IDRol
            };
            Usuario.register(usuarioData.Usuario, (err, data) => {
                if(!(data === null)) {
                    res.json({msg: "Usuario Existente"});
                }else{
                    Usuario.post(usuarioData, (err, data) => {
                        if(data && data.msg) {
                            res.json(data);
                        }else{
                            res.json({msg: "Error Al Insertar"})
                        }
                    });
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });
    
    app.post('/upload/usuario', (req, res, next) => {
        //Parseamos la Imagen
        var form = new formidable.IncomingForm();
        form.keepExtensions = true;
        form.maxFieldsSize = 10 * 1024 * 1024; //10 MB
        form.multiples = true;
        form.parse(req, (err, fields, files) => {
            if (err) {
                res.json({
                    result: "failed",
                    data: {},
                    messege: `No se pudo subir la imagen.Error is : ${err}`
                });
            }
            //console.log(files)
            var arrayOfFiles = [];
            if(files[""] instanceof Array) {
                arrayOfFiles = files[""];
            } else {
                arrayOfFiles.push(files[""]);
            }         
            if (arrayOfFiles.length > 0) {
                //Cambiamos de ruta la imagen
                fse.copy(files.Imagen.path, 'src/images/usuarios/'+ files.Imagen.name, err => {
                    if (err) return console.error(err)
                    //console.log('Exito al Subir la Imagen!')
                });
                const usuarioData = {
                    IDUsuario: fields.IDUsuario,
                    Imagen: files.Imagen.name
                };
                Usuario.insertImagen(usuarioData, (err, data) => {
                    //console.log(data)
                    if(data && data.msg) {
                        //console.log('Imagen Insertada');
                    }else {
                        //console.log('Error Al Insertar la Imagen')
                    }
                });
                res.json({
                    result: "Exito",
                    data: files.name,
                    messege: "Imagen Subida Correctamente",
                    files,
                    fields
                });
            } else {
                res.json({
                    result: "Error",
                    data: {},
                    numberOfImages: 0,
                    messege: "Imagen no Subida!"
                });
            }
        });
    });

    app.put('/usuarios/:IDUsuario', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const usuarioData = {
                IDUsuario: req.params.IDUsuario,
                NombreUsuario: req.body.NombreUsuario,
                ApellidoUsuario: req.body.ApellidoUsuario,
                DireccionUsuario: req.body.DireccionUsuario,
                TelefonoUsuario: req.body.TelefonoUsuario,
                CorreoUsuario: req.body.CorreoUsuario,        
                Contrasena: req.body.Contrasena,
                Estado: req.body.Estado,
                Codigo: req.body.Codigo,
                IDRol: req.body.IDRol
            };
            Usuario.put(usuarioData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/usuarios2/:IDUsuario', (req, res) => {
        const usuarioData = {
            IDUsuario: req.params.IDUsuario,
            Estado: req.body.Estado,
            Contrasena: req.body.Contrasena,
            Codigo: req.body.Codigo
        };
        Usuario.put2(usuarioData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    msg: "Error Al Editar"
                })
            }
        });
    });

    app.delete('/usuarios/:IDUsuario', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Usuario.delete(req.params.IDUsuario, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.post('/login', (req, res) => {
        const usuarioData = {
            Usuario: req.body.Usuario,
            Contrasena: req.body.Contrasena
        }   
        Usuario.login(usuarioData.Usuario, usuarioData.Contrasena, (err, data) => {
            if(!(data === null)) {
                var token = jwt.sign(JSON.parse(JSON.stringify(data)), "Contrasena", {expiresIn: "1d"});
                res.json({"Usuario" : JSON.parse(JSON.stringify(data)), "Token" : token});
            }else{
                res.json({msg: "No Encontrado"});
            }
        });
    });

    app.post('/login2', (req, res) => {
        const usuarioData = {
            Usuario: req.body.Usuario,
            Codigo: req.body.Codigo
        } 
        Usuario.login2(usuarioData.Usuario, usuarioData.Codigo, (err, data) => {
            if(!(data === null)) {
                var token = jwt.sign(JSON.parse(JSON.stringify(data)), "Codigo", {expiresIn: "1d"});
                res.json({"Usuario" : JSON.parse(JSON.stringify(data)), "Token" : token});
            }else{
                res.json({msg: "No Encontrado"});
            }
        });
    });

    app.post('/register', (req, res) => {
        const usuarioData = {
            IDUsuario: req.body.IDUsuario,
            NombreUsuario: req.body.NombreUsuario,
            ApellidoUsuario: req.body.ApellidoUsuario,
            DireccionUsuario: req.body.DireccionUsuario,
            TelefonoUsuario: req.body.TelefonoUsuario,
            CorreoUsuario: req.body.CorreoUsuario,                    
            Usuario: req.body.Usuario,
            Contrasena: req.body.Contrasena,
            Estado: req.body.Estado,
            Codigo: req.body.Codigo,
            IDRol: req.body.IDRol
        };  
        Usuario.register(usuarioData.Usuario, (err, data) => {
            if(!(data === null)) {
                res.json({msg: "Usuario Existente"});
            }else{
                Usuario.post(usuarioData, (err, data) => {
                    if(data && data.msg) {
                        res.json(data);
                    }else{
                        res.json({msg: "Error Al Registrarse"})
                    }
                });
            }
        });
    });

}