const Pedidos = require('../models/PedidosModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/pedidos', (req, res) => {
        e = auth.Verificar(req);        
        if(e == undefined) {
            Pedidos.get((err, data) => {
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.post('/pedidos', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const pedidosData = {
                CodigoPedido: req.body.CodigoPedido,
                IDEstado: req.body.IDEstado,
                IDUsuario: req.body.IDUsuario
            }
            Pedidos.post(pedidosData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/pedidos/:IDPedido', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const pedidosData = {
                IDPedido: req.params.IDPedido,
                CodigoPedido: req.body.CodigoPedido,
                IDEstado: req.body.IDEstado,
                IDUsuario: req.body.IDUsuario
            };
            Pedidos.put(pedidosData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/pedidos/:IDPedido', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Pedidos.delete(req.params.IDPedido, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

}