const Comentarios = require('../models/ComentariosModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/comentarios', (req, res) => {
        Comentarios.get((err, data) => {
            res.status(200).json(data);
        });
    });

    app.post('/comentarios', (req, res) => {
        const comentariosData = {
            Comentario: req.body.Comentario,
            Estrellas: req.body.Estrellas,
            IDProducto: req.body.IDProducto,
            IDUsuario: req.body.IDUsuario
        }
        Comentarios.post(comentariosData, (err, data) => {
            if(data && data.msg) {
                res.json(data);
            }else{
                res.json({msg: "Error Al Insertar"})
            }
        });
    });

    app.put('/comentarios/:IDComentario', (req, res) => {
        const comentariosData = {
            IDComentario: req.params.IDComentario,
            Comentario: req.body.Comentario,
            Estrellas: req.body.Estrellas,
            IDProducto: req.body.IDProducto,
            IDUsuario: req.body.IDUsuario
        };
        Comentarios.put(comentariosData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    msg: "Error Al Editar"
                })
            }
        });
    });

    app.delete('/comentarios/:IDComentario', (req, res) => {
        Comentarios.delete(req.params.IDComentario, (err, data) => {
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    msg: "Error Al Eliminar"
                })
            }
        });
    });
}