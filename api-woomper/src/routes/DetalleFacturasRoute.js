const DetalleFacturas = require('../models/DetalleFacturasModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/detalleFacturas', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            DetalleFacturas.get((err, data) => {
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.post('/detalleFacturas', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const detallefacturasData = {
                IDProducto: req.body.IDProducto,
                Cantidad: req.body.Cantidad,
                SubTotal: req.body.SubTotal,
                IDFactura: req.body.IDFactura
            }
            DetalleFacturas.post(detallefacturasData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/detalleFacturas/:IDDetalleFactura', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const detallefacturasData = {
                IDDetalleFactura: req.params.IDDetalleFactura,
                IDProducto: req.body.IDProducto,
                Cantidad: req.body.Cantidad,
                SubTotal: req.body.SubTotal,
                IDFactura: req.body.IDFactura
            };
            DetalleFacturas.put(detallefacturasData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/detalleFacturas/:IDDetalleFactura', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            DetalleFacturas.delete(req.params.IDDetalleFactura, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

}