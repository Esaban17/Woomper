const Facturas = require('../models/FacturasModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/facturas', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Facturas.get((err, data) => {
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.post('/facturas', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const facturasData = {
                CodigoFactura: req.body.CodigoFactura,
                Total: req.body.Total,
                IDPedido: req.body.IDPedido
            }
            Facturas.post(facturasData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/facturas/:IDFactura', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const facturasData = {
                IDFactura: req.params.IDFactura,
                CodigoFactura: req.body.CodigoFactura,
                Total: req.body.Total,
                IDPedido: req.body.IDPedido
            };
            Facturas.put(facturasData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/facturas/:IDFactura', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Facturas.delete(req.params.IDFactura, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

}