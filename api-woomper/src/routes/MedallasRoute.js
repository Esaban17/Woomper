const Medallas = require('../models/MedallasModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/medallas', (req, res) => {
        e = auth.Verificar(req);        
        if(e == undefined) {
            Medallas.get((err, data) => {
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.post('/medallas', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const medallasData = {
                Medalla: req.body.Medalla,
                Estado: req.body.Estado,
                IDUsuario: req.body.IDUsuario
            }
            Medallas.post(medallasData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/medallas/:IDMedalla', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const medallasData = {
                IDMedalla: req.params.IDMedalla,
                Medalla: req.body.Medalla,
                Estado: req.body.Estado,
                IDUsuario: req.body.IDUsuario
            };
            Medallas.put(medallasData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/medallas/:IDMedalla', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Medallas.delete(req.params.IDMedalla, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

}