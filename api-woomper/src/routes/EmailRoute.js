const nodemailer = require('nodemailer');
const path = require('path');
module.exports = function(app){

    //Correos a Usuarios

    app.post('/email/contacto-user', (req, res) => {
        // CREAMOS EL FORMULARIO PARA ENVIAR EL COMENTARIO
        const output = `
        <h3>¡Hola ${req.body.NombreUsuario} ${req.body.ApellidoUsuario}!</h3>
        <p>Tu mensaje de contacto ha sido recibido</p>
        <p>Pronto nos pondremos en contacto contigo</p>
        <br>
        <p>Equipo de Woomper...</p>`;
  
        // CREAMOS EL TRANSPORTE CON EL PROTOCOLO SMTP
        var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'woompergt@gmail.com',
            pass: 'Woomper@18'
            }
        });
    
        // CONFIGURACION PARA EL CORREO
        let mailOptions = {
            from: '<woompergt@gmail.com>', // EMISOR
            to: `${req.body.CorreoUsuario}`, // RECEPTORES
            subject: 'Woomper - Contacto', // Subject line
            text: 'Contacto', // plain text body
            html: output // VISTA HTML
        };
    
        // DECLARAMOS EL TRANSPORTE Y ENVIAMOS EL CORREO
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }else{
                return res.json({msg:'Correo Enviado Con Exito'});
            }

        });
    });

    app.post('/email/bienvenido', (req, res) => {
        // CREAMOS EL FORMULARIO PARA ENVIAR EL COMENTARIO
        const output = `
        <h3>¡Hola ${req.body.NombreUsuario} ${req.body.ApellidoUsuario}!</h3>
        <p>Te damos la bienvenida a Woomper</p>
        <p>Es un gusto tenerte con nosotros</p>
        <br>
        <p>Equipo de Woomper...</p>`;
  
        // CREAMOS EL TRANSPORTE CON EL PROTOCOLO SMTP
        var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'woompergt@gmail.com',
            pass: 'Woomper@18'
            }
        });
    
        // CONFIGURACION PARA EL CORREO
        let mailOptions = {
            from: '<woompergt@gmail.com>', // EMISOR
            to: `${req.body.CorreoUsuario}`, // RECEPTORES
            subject: 'Bienvenido a Woomper', // Subject line
            text: 'Bienvenido', // plain text body
            html: output // VISTA HTML
        };
    
        // DECLARAMOS EL TRANSPORTE Y ENVIAMOS EL CORREO
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }else{
                return res.json({msg:'Correo Enviado Con Exito'});
            }

        });
    });

    app.post('/email/verificar', (req, res) => {
        // CREAMOS EL FORMULARIO PARA ENVIAR EL COMENTARIO
        const output = `
        <h3>¡Hola ${req.body.NombreUsuario} ${req.body.ApellidoUsuario}!</h3>
        <p>Tu codigo de verificacion es ${req.body.Codigo}</p>
        <br>
        <p>Equipo de Woomper...</p>`;
  
        // CREAMOS EL TRANSPORTE CON EL PROTOCOLO SMTP
        var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'woompergt@gmail.com',
            pass: 'Woomper@18'
            }
        });
    
        // CONFIGURACION PARA EL CORREO
        let mailOptions = {
            from: '<woompergt@gmail.com>', // EMISOR
            to: `${req.body.CorreoUsuario}`, // RECEPTORES
            subject: 'Woomper - Verificacion', // Subject line
            text: 'Verificacion', // plain text body
            html: output // VISTA HTML
        };
    
        // DECLARAMOS EL TRANSPORTE Y ENVIAMOS EL CORREO
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }else{
                return res.json({msg:'Correo Enviado Con Exito'});
            }

        });
    });

    app.post('/email/pedido-user', (req, res) => {
        // CREAMOS EL FORMULARIO PARA ENVIAR EL COMENTARIO
        const output = `
        <h3>¡Hola ${req.body.NombreUsuario} ${req.body.ApellidoUsuario}!</h3>
        <p>Tu pedido "${req.body.CodigoPedido}" se encuentra en estado ${req.body.Estado}(%${req.body.Porcentaje})</p>
        <br>
        <p>Equipo de Woomper...</p>`;
  
        // CREAMOS EL TRANSPORTE CON EL PROTOCOLO SMTP
        var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'woompergt@gmail.com',
            pass: 'Woomper@18'
            }
        });
    
        // CONFIGURACION PARA EL CORREO
        let mailOptions = {
            from: '<woompergt@gmail.com>', // EMISOR
            to: `${req.body.CorreoUsuario}`, // RECEPTORES
            subject: `Pedido ${req.body.CodigoPedido}`, // Subject line
            text: `Pedido ${req.body.CodigoPedido}`, // plain text body
            html: output // VISTA HTML
        };
    
        // DECLARAMOS EL TRANSPORTE Y ENVIAMOS EL CORREO
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }else{
                return res.json({msg:'Correo Enviado Con Exito'});
            }

        });
    });

    //Correos a Administrador

    app.post('/email/contacto-admin', (req, res) => {
        // CREAMOS EL FORMULARIO PARA ENVIAR EL COMENTARIO
        const output = `
        <h3>Nuevo Mensaje de Contacto</h3>
        <p>De: ${req.body.NombreUsuario} ${req.body.ApellidoUsuario}</p>
        <p>Correo: ${req.body.CorreoUsuario}</p>
        <p>Mensaje: ${req.body.Mensaje}</p>
        <br>
        <p>Equipo de Woomper...</p>`;
  
        // CREAMOS EL TRANSPORTE CON EL PROTOCOLO SMTP
        var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'woompergt@gmail.com',
            pass: 'Woomper@18'
            }
        });
    
        // CONFIGURACION PARA EL CORREO
        let mailOptions = {
            from: '<woompergt@gmail.com>', // EMISOR
            to: 'woompergt@gmail.com', // RECEPTORES
            subject: 'Woomper - Contacto', // Subject line
            text: 'Contacto', // plain text body
            html: output // VISTA HTML
        };
    
        // DECLARAMOS EL TRANSPORTE Y ENVIAMOS EL CORREO
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }else{
                return res.json({msg:'Correo Enviado Con Exito'});
            }

        });
    });

    app.post('/email/nuevo-usuario', (req, res) => {
        // CREAMOS EL FORMULARIO PARA ENVIAR EL COMENTARIO
        const output = `
        <h3>¡Nuevo Usuario Registrado!</h3>
        <p>Nombre: ${req.body.NombreUsuario} ${req.body.ApellidoUsuario}</p>
        <p>CorreoElectronico: ${req.body.CorreoUsuario}</p>
        <br>
        <p>Equipo de Woomper...</p>`;
  
        // CREAMOS EL TRANSPORTE CON EL PROTOCOLO SMTP
        var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'woompergt@gmail.com',
            pass: 'Woomper@18'
            }
        });
    
        // CONFIGURACION PARA EL CORREO
        let mailOptions = {
            from: '<woompergt@gmail.com>', // EMISOR
            to: 'woompergt@gmail.com', // RECEPTORES
            subject: 'Nuevo Usuario en Woomper', // Subject line
            text: 'Bienvenido', // plain text body
            html: output // VISTA HTML
        };
    
        // DECLARAMOS EL TRANSPORTE Y ENVIAMOS EL CORREO
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }else{
                return res.json({msg:'Correo Enviado Con Exito'});
            }

        });
    });

    app.post('/email/pedido-admin', (req, res) => {
        // CREAMOS EL FORMULARIO PARA ENVIAR EL COMENTARIO
        const output = `
        <h3>¡Nuevo Pedido!</h3>
        <p>Usuario: ${req.body.NombreUsuario} ${req.body.ApellidoUsuario}</p>
        <p>CorreoElectronico: ${req.body.CorreoUsuario}</p>
        <p>Pedido: ${req.body.CodigoPedido}</p>
        <br>
        <p>Equipo de Woomper...</p>`;
  
        // CREAMOS EL TRANSPORTE CON EL PROTOCOLO SMTP
        var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'woompergt@gmail.com',
            pass: 'Woomper@18'
            }
        });
    
        // CONFIGURACION PARA EL CORREO
        let mailOptions = {
            from: '<woompergt@gmail.com>', // EMISOR
            to: 'woompergt@gmail.com', // RECEPTORES
            subject: 'Nuevo Pedido en Woomper', // Subject line
            text: 'Pedidos', // plain text body
            html: output // VISTA HTML
        };
    
        // DECLARAMOS EL TRANSPORTE Y ENVIAMOS EL CORREO
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }else{
                return res.json({msg:'Correo Enviado Con Exito'});
            }

        });
    });
}