const Notificaciones = require('../models/NotificacionesModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/notificaciones', (req, res) => {
        e = auth.Verificar(req);        
        if(e == undefined) {
            Notificaciones.get((err, data) => {
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.post('/notificaciones', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const notificacionesData = {
                Notificacion: req.body.Notificacion,
                Tipo: req.body.Tipo,
                IDUsuario: req.body.IDUsuario
            }
            Notificaciones.post(notificacionesData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/notificaciones/:IDNotificacion', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const notificacionesData = {
                IDNotificacion: req.params.IDNotificacion,
                Notificacion: req.body.Notificacion,
                Tipo: req.body.Tipo,
                IDUsuario: req.body.IDUsuario
            };
            Notificaciones.put(notificacionesData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/notificaciones/:IDNotificacion', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Notificaciones.delete(req.params.IDNotificacion, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

}