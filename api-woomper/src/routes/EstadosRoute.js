const Estados = require('../models/EstadosModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app) {

    app.get('/estados/', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Estados.get((err, data) => {
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });
    
}
