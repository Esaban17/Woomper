const Roles = require('../models/RolesModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

module.exports = function(app) {

    app.get('/roles', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Roles.get((err, data) => {
                res.status(200).json(data);
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

}