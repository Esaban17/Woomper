const Categorias = require('../models/CategoriasModel');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

var e = false;

module.exports = function(app){

    app.get('/categorias', (req, res) => {
        Categorias.get((err, data) =>{
            res.status(200).json(data);
        });
    });

    app.post('/categorias', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const categoriasData = {
                Categoria: req.body.Categoria
            }
            Categorias.post(categoriasData, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else{
                    res.json({msg: "Error Al Insertar"})
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.put('/categorias/:IDCategoria', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            const categoriasData = {
                IDCategoria: req.params.IDCategoria,
                Categoria: req.body.Categoria
            };
            Categorias.put(categoriasData, (err, data) =>{
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Editar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });

    app.delete('/categorias/:IDCategoria', (req, res) => {
        e = auth.Verificar(req);
        if(e == undefined) {
            Categorias.delete(req.params.IDCategoria, (err, data) => {
                if(data && data.msg) {
                    res.json(data);
                }else {
                    res.json({
                        msg: "Error Al Eliminar"
                    })
                }
            });
        }else{
            res.json({msg: "Token Invalido"});
            e = false;
        }
    });
    
}