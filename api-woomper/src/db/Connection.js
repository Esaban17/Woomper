const mysql = require('mysql');

connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "woomper_db"
    //socketPath : '/Applications/MAMP/tmp/mysql/mysql.sock'
});

connection.connect(function(err) {
    if (err) throw err;
    else console.log("Connected!");
});

module.exports = connection;
