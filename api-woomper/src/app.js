const express = require('express');
const app = express();
const bodyParser = require('body-parser');

//Settings
app.set('port', process.env.PORT || 3000);

//Middlewares
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
	next();
});

require('./routes/RolesRoute')(app);
require('./routes/UsuariosRoute')(app);
require('./routes/EstadosRoute')(app);
require('./routes/SucursalesRoute')(app);
require('./routes/CategoriasRoute')(app);
require('./routes/ProveedoresRoute')(app);
require('./routes/ProductosRoute')(app);
require('./routes/ComentariosRoute')(app);
require('./routes/FacturasRoute')(app);
require('./routes/DetallefacturasRoute')(app);
require('./routes/CuponesRoute')(app);
require('./routes/TarjetasRoute')(app);
require('./routes/ChatsRoute')(app);
require('./routes/PedidosRoute')(app);
require('./routes/SubCategoriasRoute')(app);
require('./routes/NotificacionesRoute')(app);
require('./routes/MedallasRoute')(app);
require('./routes/EmailRoute')(app);

app.listen(app.get('port'), () => {
    console.log(`Server run in port ${app.get('port')}`);
});