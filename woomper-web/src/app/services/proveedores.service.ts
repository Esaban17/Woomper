import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app/app.component';

@Injectable()
export class ProveedoresService {

  private rutaProveedores = "http://localhost:3000/proveedores/";

  headers = new Headers();
  constructor( private http: Http ) { 
      this.headers.append("Content-Type", "application/json");
      this.headers.append("Authorization", AppComponent.UL[0].Token);
  }

  getProveedores(){
    return this.http.get(this.rutaProveedores, {headers: this.headers });
  }

  postProveedores( obj: any ){
    const body = obj;
    return this.http.post(this.rutaProveedores, body, {headers: this.headers });
  }

  putProveedores( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaProveedores+id, body, {headers: this.headers });
  }

  deleteProveedores( id: any ){
    return this.http.delete(this.rutaProveedores+id, {headers: this.headers });
  }

}
