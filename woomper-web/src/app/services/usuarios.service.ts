import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AppComponent } from '../../app/app.component';

@Injectable()
export class UsuariosService {

  private rutaUsuarios = "http://localhost:3000/usuarios/";
  private rutaRoles = "http://localhost:3000/roles/";
  private rutaUpload = 'http://localhost:3000/upload/usuario/';
  private rutaImagen = 'http://localhost:3000/usuarios/imagen/';

  headers = new Headers();
  constructor( private http: Http ) { 
    this.headers.append("Content-Type", "application/json");
    this.headers.append("Authorization", AppComponent.UL[0].Token); 
  }

  getRoles(){
    return this.http.get(this.rutaRoles, {headers: this.headers });
  }

  getImagen( imagen: string){
    return this.http.get(this.rutaImagen+imagen );
  }

  getUsuarios(){
    return this.http.get(this.rutaUsuarios, {headers: this.headers });
  }

  postUsuarios( obj: any ){
    const body = obj;
    return this.http.post(this.rutaUsuarios, body, {headers: this.headers });
  }

  putUsuarios( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaUsuarios+id, body, {headers: this.headers });
  }

  deleteUsuarios( id: any ){
    return this.http.delete(this.rutaUsuarios+id, {headers: this.headers });
  }

  subirPortada(IDUsuario,file){
    let formData = new FormData();
    formData.append('Imagen', file);
    formData.append('IDUsuario', IDUsuario);
    return this.http.post(this.rutaUpload, formData);
  }

}
