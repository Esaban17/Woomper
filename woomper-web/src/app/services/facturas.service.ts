import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app/app.component';

@Injectable()
export class FacturasService {

  private rutaFacturas = "http://localhost:3000/facturas/";

  headers = new Headers();
  constructor( private http: Http ) { 
    this.headers.append("Content-Type", "application/json");
    this.headers.append("Authorization", AppComponent.UL[0].Token);
  }

  getFacturas(){
    return this.http.get(this.rutaFacturas, {headers: this.headers });
  }

  postFacturas( obj: any ){
    const body = obj;
    return this.http.post(this.rutaFacturas, body, {headers: this.headers });
  }

  putFacturas( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaFacturas+id, body, {headers: this.headers });
  }

  deleteFacturas( id: any ){
    return this.http.delete(this.rutaFacturas+id, {headers: this.headers });
  }

}
