import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app/app.component';

@Injectable()
export class ProductosService {

  private rutaProductos = "http://localhost:3000/productos/";
  private rutaUpload = 'http://localhost:3000/upload/producto/';
  private rutaImagen = 'http://localhost:3000/productos/imagen/';

  headers = new Headers();
  constructor( private http: Http ) { 
    this.headers.append("Content-Type", "application/json");
    this.headers.append("Authorization", AppComponent.UL[0].Token);
  }

  getProductos(){
    return this.http.get(this.rutaProductos);
  }

  getImagen( imagen: string){
    return this.http.get(this.rutaImagen+imagen );
  }

  postProductos( obj: any ){
    const body = obj;
    return this.http.post(this.rutaProductos, body, {headers: this.headers });
  }

  putProductos( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaProductos+id, body, {headers: this.headers });
  }

  deleteProductos( id: any ){
    return this.http.delete(this.rutaProductos+id, {headers: this.headers });
  }

  subirPortada(IDProducto,file){
    let formData = new FormData();
    formData.append('Imagen', file);
    formData.append('IDProducto', IDProducto);
    return this.http.post(this.rutaUpload, formData);
  }

}
