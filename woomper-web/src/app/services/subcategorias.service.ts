import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app/app.component';

@Injectable()
export class SubcategoriasService {

  private rutaSubCategorias = "http://localhost:3000/subcategorias/";

  headers = new Headers();
  constructor( private http: Http ) { 
    this.headers.append("Content-Type", "application/json");
    this.headers.append("Authorization", AppComponent.UL[0].Token);
  }

  getSubCategorias(){
    return this.http.get(this.rutaSubCategorias);
  }

  postSubCategorias( obj: any ){
    const body = obj;
    return this.http.post(this.rutaSubCategorias, body, {headers: this.headers });
  }

  putSubCategorias( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaSubCategorias+id, body, {headers: this.headers });
  }

  deleteSubCategorias( id: any ){
    return this.http.delete(this.rutaSubCategorias+id, {headers: this.headers });
  }

}
