import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app/app.component';
@Injectable()
export class AutenticacionService {

  private rutaLogin = 'http://localhost:3000/login';
  private rutaLogin2 = 'http://localhost:3000/login2';
  private rutaRegister = 'http://localhost:3000/register';
  private rutaUsuarios = "http://localhost:3000/usuarios2/";

  constructor( private http: Http ) { }

  private headers(): Headers{
    let headers = new Headers({'Content-Type': 'application/json'});
    headers.append('Authorization', AppComponent.UL[0].Token);
    return headers;
  }

  login(userData): Observable<any>{
    return this.http.post(this.rutaLogin, userData);
  }

  login2(userData): Observable<any>{
    const body = userData;
    return this.http.post(this.rutaLogin2, body);
  }

  register(userData): Observable<any>{
    return this.http.post(this.rutaRegister, userData);
  }

  cambiar( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaUsuarios+id, body);
  }

  getUsuarios(){
    return this.http.get(this.rutaUsuarios);
  }

}
