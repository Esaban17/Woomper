import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  private rutaEmailContacto = "http://localhost:3000/email/contacto-user";
  private rutaEmailContacto2 = "http://localhost:3000/email/contacto-admin";
  private rutaEmailBienvenido = "http://localhost:3000/email/bienvenido";
  private rutaEmailNuevoUsuario = "http://localhost:3000/email/nuevo-usuario";
  private rutaEmailVerificar = "http://localhost:3000/email/verificar";
  private rutaEmailPedido = "http://localhost:3000/email/pedido-user";
  private rutaEmailPedido2 = "http://localhost:3000/email/pedido-admin";

  constructor(private http: Http) { }

  emailContacto( obj: any ){
    const body = obj;
    return this.http.post(this.rutaEmailContacto, body);
  }

  emailContacto2( obj: any ){
    const body = obj;
    return this.http.post(this.rutaEmailContacto2, body);
  }

  emailBienvenido( obj: any ){
    const body = obj;
    return this.http.post(this.rutaEmailBienvenido, body);
  }

  emailNuevoUsuario( obj: any ){
    const body = obj;
    return this.http.post(this.rutaEmailNuevoUsuario, body);
  }

  emailVerificar( obj: any ){
    const body = obj;
    return this.http.post(this.rutaEmailVerificar, body);
  }

  emailPedido( obj: any ){
    const body = obj;
    return this.http.post(this.rutaEmailPedido, body);
  }

  emailPedido2( obj: any ){
    const body = obj;
    return this.http.post(this.rutaEmailPedido2, body);
  }

}
