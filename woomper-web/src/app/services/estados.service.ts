import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app/app.component';

@Injectable()
export class EstadosService {

  private rutaEstados = "http://localhost:3000/estados/";

  headers = new Headers();
  constructor( private http: Http ) { 
    this.headers.append("Content-Type", "application/json");
    this.headers.append("Authorization", AppComponent.UL[0].Token);
  }

  getEstados(){
    return this.http.get(this.rutaEstados, {headers: this.headers });
  }

}
