import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class TiendaService {

  private rutaProductos = "http://localhost:3000/productos/";
  private rutaCategorias = "http://localhost:3000/categorias/";
  private rutaSucursales = "http://localhost:3000/sucursales/";
  private rutaSubCategorias = "http://localhost:3000/subcategorias/";
  private rutaCantUsuarios = "http://localhost:3000/cantusuarios/";
  private rutaUsuarios = "http://localhost:3000/usuarios2/";
  private rutaComentarios = "http://localhost:3000/comentarios/";

  constructor( private http: Http ) {

  }

  getProductos(){
    return this.http.get(this.rutaProductos);
  }

  getCategorias(){
    return this.http.get(this.rutaCategorias);
  }

  getSucursales(){
    return this.http.get(this.rutaSucursales);
  }

  getSubCategorias(){
    return this.http.get(this.rutaSubCategorias);
  }

  getUsuarios(){
    return this.http.get(this.rutaUsuarios);
  }

  getCantUsuarios(){
    return this.http.get(this.rutaCantUsuarios);
  }

  getComentarios(){
    return this.http.get(this.rutaComentarios);
  }

  postComentarios( obj: any ){
    const body = obj;
    return this.http.post(this.rutaComentarios, body);
  }

  putComentarios( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaComentarios+id, body);
  }

  deleteComentarios( id: any ){
    return this.http.delete(this.rutaComentarios+id);
  }

}
