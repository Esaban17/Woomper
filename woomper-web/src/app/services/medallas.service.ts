import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { AppComponent } from '../../app/app.component';

@Injectable()
export class MedallasService {

  private rutaMedallas = "http://localhost:3000/medallas/";

  headers = new Headers();
  constructor( private http: Http ) { 
    this.headers.append("Content-Type", "application/json");
    this.headers.append("Authorization", AppComponent.UL[0].Token); 
  }

  getMedallas(){
    return this.http.get(this.rutaMedallas);
  }

  postMedallas( obj: any ){
    const body = obj;
    return this.http.post(this.rutaMedallas, body, {headers: this.headers });
  }

  putMedallas( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaMedallas+id, body, {headers: this.headers });
  }

  deleteMedallas( id: any ){
    return this.http.delete(this.rutaMedallas+id, {headers: this.headers });
  }

}
