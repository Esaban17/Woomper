import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class VerificarService {

  private rutaUsuarios = "http://localhost:3000/usuarios2/";

  constructor(private http: Http) { }

  putUsuarios( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaUsuarios+id, body);
  }

}
