import { Injectable } from '@angular/core';
import { Http, Headers, } from '@angular/http';
import { AppComponent } from '../app.component';

@Injectable()
export class PedidosService {

  private rutaPedidos = "http://localhost:3000/pedidos/";

  headers = new Headers();
  constructor( private http: Http ) { 
    this.headers.append("Content-Type", "application/json");
    this.headers.append("Authorization", AppComponent.UL[0].Token);
    
  }

  getPedidos(){
    return this.http.get(this.rutaPedidos, {headers: this.headers });
  }

  postPedidos( obj: any ){
    const body = obj;   
    return this.http.post(this.rutaPedidos, body, {headers: this.headers });
  }

  putPedidos( id: any, obj: any ){
    const body = obj;    
    return this.http.put(this.rutaPedidos+id, body, {headers: this.headers });
  }

  deletePedidos( id: any ){
    return this.http.delete(this.rutaPedidos+id, {headers: this.headers });
  }

}
