import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app/app.component';

@Injectable()
export class CategoriasService {

  private rutaCategorias = "http://localhost:3000/categorias/";

  headers = new Headers();
  constructor( private http: Http ) { 
    this.headers.append("Content-Type", "application/json");
    this.headers.append("Authorization", AppComponent.UL[0].Token);
  }

  getCategorias(){
    return this.http.get(this.rutaCategorias);
  }

  postCategorias( obj: any ){
    const body = obj;
    return this.http.post(this.rutaCategorias, body, {headers: this.headers });
  }

  putCategorias( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaCategorias+id, body, {headers: this.headers });
  }

  deleteCategorias( id: any ){
    return this.http.delete(this.rutaCategorias+id, {headers: this.headers });
  }

}
