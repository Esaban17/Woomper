import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app/app.component';

@Injectable()
export class SucursalesService {

  private rutaSucursales = "http://localhost:3000/sucursales/";

  headers = new Headers();
  
  constructor( private http: Http ) { 
    if (AppComponent.UL[0] != null) {
      this.headers.append("Content-Type", "application/json");
      this.headers.append("Authorization", AppComponent.UL[0].Token);
    }
  }

  getSucursales(){
    return this.http.get(this.rutaSucursales);
  }

  postSucursales( obj: any ){
    const body = obj;
    return this.http.post(this.rutaSucursales, body, {headers: this.headers });
  }

  putSucursales( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaSucursales+id, body, {headers: this.headers });
  }

  deleteSucursales( id: any ){
    return this.http.delete(this.rutaSucursales+id, {headers: this.headers });
  }

}
