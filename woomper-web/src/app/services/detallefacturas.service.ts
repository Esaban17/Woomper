import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app/app.component';

@Injectable()
export class DetallefacturasService {

  private rutaDetalleFacturas = "http://localhost:3000/detallefacturas/";

  headers = new Headers();
  constructor( private http: Http ) { 
    if (AppComponent.UL[0] != null) {
      this.headers.append("Content-Type", "application/json");
      this.headers.append("Authorization", AppComponent.UL[0].Token);
    }
  }

  getDetalleFacturas(){
    return this.http.get(this.rutaDetalleFacturas, {headers: this.headers });
  }

  postDetalleFacturas( obj: any ){
    const body = obj;
    return this.http.post(this.rutaDetalleFacturas, body, {headers: this.headers });
  }

  putDetalleFacturas( id: any, obj: any ){
    const body = obj;
    return this.http.put(this.rutaDetalleFacturas+id, body, {headers: this.headers });
  }

  deleteDetalleFacturas( id: any ){
    return this.http.delete(this.rutaDetalleFacturas+id, {headers: this.headers });
  }

}
