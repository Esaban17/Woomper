import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app/app.component';

@Injectable({
  providedIn: 'root'
})
export class ChatsService {

  private rutaChats = "http://localhost:3000/chats/";

  headers = new Headers();
  constructor( private http: Http ) { 

  }

  getChats(){
    return this.http.get(this.rutaChats);
  }

  postChats( obj: any ){
    const body = obj;
    return this.http.post(this.rutaChats, body);
  }

  deleteChats( id: any ){
    return this.http.delete(this.rutaChats+id);
  }

}
