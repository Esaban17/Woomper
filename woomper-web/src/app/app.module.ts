import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { APP_ROUTING } from './app.routing';

import { AppComponent } from './app.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { TiendaComponent } from './components/tienda/tienda.component';
import { HomeComponent } from './components/home/home.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { RegistroComponent } from './components/registro/registro.component';
import { LoginComponent } from './components/login/login.component';
import { UsuariosComponent } from './components/panel/usuarios/usuarios.component';

import { AutenticacionService } from '../app/services/autenticacion.service';
import { UsuariosService } from './services/usuarios.service';
import { PanelComponent } from './components/panel/panel.component';
import { MenuComponent } from './components/panel/menu/menu.component';
import { CategoriasComponent } from './components/panel/categorias/categorias.component';
import { CategoriasService } from './services/categorias.service';
import { SubcategoriasComponent } from './components/panel/subcategorias/subcategorias.component';
import { SubcategoriasService } from './services/subcategorias.service';
import { SucursalesComponent } from './components/panel/sucursales/sucursales.component';
import { SucursalesService } from './services/sucursales.service';
import { ProveedoresComponent } from './components/panel/proveedores/proveedores.component';
import { ProveedoresService } from './services/proveedores.service';
import { ProductosComponent } from './components/panel/productos/productos.component';
import { PedidosComponent } from './components/panel/pedidos/pedidos.component';
import { ProductosService } from './services/productos.service';
import { PedidosService } from './services/pedidos.service';
import { TiendaService } from './services/tienda.service';
import { FacturasService } from './services/facturas.service';
import { CarritoComponent } from './components/carrito/carrito.component';
import { CajaComponent } from './components/caja/caja.component';
import { DetallefacturasService } from './services/detallefacturas.service';
import { EstadosService } from './services/estados.service';
import { ContactoComponent } from './components/contacto/contacto.component';
import { FaqsComponent } from './components/faqs/faqs.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { VerificarService } from './services/verificar.service';
import { EmailService } from './services/email.service';
import { ChatsService } from './services/chats.service';
import { AyudaComponent } from './components/ayuda/ayuda.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    HeaderComponent,
    FooterComponent,
    TiendaComponent,
    HomeComponent,
    PerfilComponent,
    RegistroComponent,
    LoginComponent,
    UsuariosComponent,
    PanelComponent,
    MenuComponent,
    CategoriasComponent,
    SubcategoriasComponent,
    SucursalesComponent,
    ProveedoresComponent,
    ProductosComponent,
    PedidosComponent,
    CarritoComponent,
    CajaComponent,
    ContactoComponent,
    FaqsComponent,
    NosotrosComponent,
    AyudaComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    APP_ROUTING
  ],
  providers: [
    AutenticacionService,
    UsuariosService,
    CategoriasService,
    SubcategoriasService,
    SucursalesService,
    ProveedoresService,
    ProductosService,
    PedidosService,
    TiendaService,
    FacturasService,
    DetallefacturasService,
    EstadosService,
    VerificarService,
    EmailService,
    ChatsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
