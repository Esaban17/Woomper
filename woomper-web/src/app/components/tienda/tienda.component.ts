import { Component, OnInit, DoCheck } from '@angular/core';
import { TiendaService } from '../../services/tienda.service';
import { AppComponent } from '../../app.component';
import { AutenticacionService } from '../../services/autenticacion.service';
import { HeaderComponent } from '../header/header.component';
import { UsuariosService } from '../../services/usuarios.service';
import { LoginComponent } from '../login/login.component';
import { VerificarService } from '../../services/verificar.service';

@Component({
  selector: 'app-tienda',
  templateUrl: './tienda.component.html'
})
export class TiendaComponent implements OnInit, DoCheck{

  constructor( private tiendaService: TiendaService, private autenticacionService: AutenticacionService, private verificarService: VerificarService ) { }

  puntero: string = "puntero";
  click: boolean = false;

  Usuario: string;
  Contrasena: string;
  Contrasena2: string;
  NombreUsuario: string;
  IDSucursal: number;

  Comentario: string;
  Estrellas: number;
  IDUsuario: number;
  ImagenU: string;

  Codigo: string;
  IDProducto: number;
  Imagen: string;
  Producto: string;
  Precio: number;
  Cantidad: number;
  Fecha: string;
  Estado: string;
  IDCategoria: number;
  IDSubCategoria: number;
  data: any;
  agregado: boolean = false;
  borrado: boolean = false;
  agotado: boolean = false;

  Existe: boolean;
  Vacio: boolean;
  Fallo: boolean;
  Repetir: boolean;
  Verificar: boolean;
  Rebalsado: boolean

  listaProductos = [];
  listaCategorias = [];
  listaSubCategorias = [];
  listaSucursales = [];
  listaComentarios = [];
  listaComentariosFiltro = []
  listaUsuarios = [];
  cont: number = 0;
  cont2: number = 0;
  static data: string = null;

  filtro = [];
  filtro2 = [];

  cod: string
  user: string
  pass: string
  name: string
  lastname: string
  dire: string
  tel: string
  mail: string
  rol: number
  status: string
  id: number

  static carrito = [];
  static Total: number = 0;
  
  filtroSubCategorias = [];

  static Hora: string;

  ngOnInit() {
    TiendaComponent.data = null;
    if ( AppComponent.UL[0] == null ) {
      this.puntero = "no-puntero"
      this.click = false;
    }else{
      this.puntero = "puntero"
      this.click = true;
    }
    this.getComentarios();
    this.getProductos();
    this.getCategorias();
    this.getSubCategorias();
    this.getSucursales();
    this.getUsuarios();
  }

  ngDoCheck(){
    this.borrado = HeaderComponent.borrado
    this.Producto = HeaderComponent.producto
  }

  getUsuarios(){
    this.tiendaService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
    });
  }

  getProductos(){
    this.tiendaService.getProductos().subscribe(res => {
      this.listaProductos = res.json();
      for (this.cont = this.listaProductos.length -1; this.cont > -1; this.cont--) {
        this.filtro.push(this.listaProductos[this.cont])
      }
    });
  }

  getCategorias(){
    this.tiendaService.getCategorias().subscribe(res => {
      this.listaCategorias = res.json();
    });
  }

  getSubCategorias(){
    this.tiendaService.getSubCategorias().subscribe(res => {
      this.listaSubCategorias = res.json();
    });
  }

  getSucursales(){
    this.tiendaService.getSucursales().subscribe(res => {
      this.listaSucursales = res.json();
    });
  }

  getComentarios(){
    this.tiendaService.getComentarios().subscribe(res => {
      this.listaComentarios = res.json();
    });
  }

  filtrarSubCategorias( id: number ){
    this.filtroSubCategorias = [];
    for (let i = 0; i < this.listaSubCategorias.length; i++) {
      if (this.listaSubCategorias[i].IDCategoria == id) {
        this.filtroSubCategorias.push(this.listaSubCategorias[i])
      }
    }
  }

  AgregarAlCarrito( id: number ){
    this.Rebalsado = false
    if (this.puntero == "no-puntero" && this.click == false) {
      
    }else{
      for (let i = 0; i < this.listaProductos.length; i++) {
        if ( id == this.listaProductos[i].IDProducto ) {
          this.Producto = this.listaProductos[i].Producto
          HeaderComponent.producto = this.Producto
          if ( this.listaProductos[i].Cantidad == 0) {
            this.agotado = true;
            setTimeout( () => { 
              this.agotado = false
            }, 3000 );
          }else{
            for (let i = 0; i < TiendaComponent.carrito.length; i++) {
              if (TiendaComponent.carrito[i].IDProducto == id) {
                TiendaComponent.carrito[i].Cantidad = TiendaComponent.carrito[i].Cantidad + 1
                TiendaComponent.Total = TiendaComponent.Total + TiendaComponent.carrito[i].Precio
                return;
              }
            }
            const cart = {
              IDProducto: this.listaProductos[i].IDProducto,
              Imagen: this.listaProductos[i].Imagen,
              Producto: this.listaProductos[i].Producto,
              Precio: this.listaProductos[i].Precio,
              Cantidad: 1,
            }
            TiendaComponent.carrito.push(cart);
            this.agregado = true
            setTimeout( () => { 
              this.agregado = false
            }, 3000 );
            TiendaComponent.Total = TiendaComponent.Total +  this.listaProductos[i].Precio  
          }
        }
      }
    }
  }

  cerrarAlert(){
    this.agregado = false
    this.borrado = false
    this.agotado = false
  }

  login(){ 
    if ( this.Usuario == null || this.Contrasena == null || this.Usuario == "" || this.Contrasena == "" ) {
      this.Vacio = true;
      this.Fallo = undefined;
    }else{
      this.Vacio = false;
      this.autenticacionService.login({"Usuario":this.Usuario, "Contrasena":this.Contrasena}).subscribe(
        (data) => {          
          if (JSON.parse(data._body).msg === "No Encontrado") {
            this.Fallo = true;   
            sessionStorage.clear()      
          } else {
            AppComponent.UL[0] = JSON.parse(data._body);
            this.NombreUsuario = AppComponent.UL[0].Usuario.NombreUsuario
            this.id = AppComponent.UL[0].Usuario.IDUsuario
            this.user = AppComponent.UL[0].Usuario.Usuario
            this.pass = AppComponent.UL[0].Usuario.Contrasena
            this.cod = AppComponent.UL[0].Usuario.Codigo
            this.name = AppComponent.UL[0].Usuario.NombreUsuario
            this.lastname = AppComponent.UL[0].Usuario.ApellidoUsuario
            this.dire = AppComponent.UL[0].Usuario.DireccionUsuario
            this.tel = AppComponent.UL[0].Usuario.TelefonoUsuario
            this.mail = AppComponent.UL[0].Usuario.CorreoUsuario
            this.rol = AppComponent.UL[0].Usuario.IDRol
            if (AppComponent.UL[0].Usuario.Estado == 0) {
              this.Verificar = true
              AppComponent.UL[0] = null
              sessionStorage.clear();
            } else{
              LoginComponent.Hora = new Date().toLocaleTimeString();
              this.Fallo = false;
              this.click = true
              this.puntero = 'puntero'
              this.Verificar = undefined
            }
          }
        }
      );
    }
  }

  verificar(){
    if ( this.cod == this.Codigo) {
      const data = {
        NombreUsuario: this.name,
        ApellidoUsuario: this.lastname,
        DireccionUsuario: this.dire,
        CorreoUsuario: this.mail,
        TelefonoUsuario:this.tel,
        Contrasena: this.pass,
        Estado: 1,
        Codigo: this.cod,
        IDRol: this.rol
      };
      this.verificarService.putUsuarios(this.id, data).subscribe(
        (datos) => {
          this.Verificar = undefined
          this.Fallo = false
          this.login()
        }
      ); 
    }else{
      this.Verificar = false
      setTimeout( () => { 
        this.Verificar = true
      }, 3000 );
    }
  }

  filtrar( idP: number ){
    if (idP == 0) {
      this.getProductos();
    }
    this.filtro = [];
    for (let i = 0; i < this.listaProductos.length; i++) {
      if (this.listaProductos[i].IDSucursal == idP ) {
        this.filtro.push(this.listaProductos[i]);
      }
    }
  }

  filtrar2( idC: number ){
    if (idC == 0) {
      this.getProductos();
    }
    this.filtro = [];
    for (let i = 0; i < this.listaProductos.length; i++) {
      if (this.listaProductos[i].IDCategoria == idC ) {
        this.filtro.push(this.listaProductos[i]);
      }
    }
  }

  setData( id: number ){
    this.listaComentariosFiltro = []
    for (let i = 0; i < this.listaProductos.length; i++) {
      if ( id == this.listaProductos[i].IDProducto ) {
        this.IDProducto = this.listaProductos[i].IDProducto
        this.Producto = this.listaProductos[i].Producto
        this.Precio = this.listaProductos[i].Precio
        this.Cantidad = this.listaProductos[i].Cantidad
        this.Fecha = this.listaProductos[i].Fecha
        this.Estado = this.listaProductos[i].Estado
        this.IDCategoria = this.listaProductos[i].IDCategoria
        this.IDSubCategoria = this.listaProductos[i].IDSubCategoria
        this.IDSucursal = this.listaProductos[i].IDSucursal
        this.Imagen = this.listaProductos[i].Imagen
      }
    }
    if (AppComponent.UL[0] != null) {
      this.IDUsuario = AppComponent.UL[0].Usuario.IDUsuario 
    }
    for (let u = 0; u < this.listaUsuarios.length; u++) {
      this.ImagenU = null
      if ( this.IDUsuario == this.listaUsuarios[u].IDUsuario ) {
        this.ImagenU = this.listaUsuarios[u].Imagen
        break
      }
    }
    for (let u = 0; u < this.listaComentarios.length; u++) {
      if ( this.IDProducto == this.listaComentarios[u].IDProducto ) {
        this.listaComentariosFiltro.push(this.listaComentarios[u])
      }
    }
  }

  getCategoria( id: number ){
    for (let i = 0; i < this.listaCategorias.length; i++) {
      if (this.listaCategorias[i].IDCategoria == id) {
        return this.listaCategorias[i].Categoria
      }
    }
  }

  getImagen( id: number ){
    for (let i = 0; i < this.listaUsuarios.length; i++) {
      if (this.listaUsuarios[i].IDUsuario == id) {
        return this.listaUsuarios[i].Imagen
      }
    }
  }

  getSubCategoria( id: number ){
    for (let i = 0; i < this.listaSubCategorias.length; i++) {
      if (this.listaSubCategorias[i].IDSubCategoria == id) {
        return this.listaSubCategorias[i].SubCategoria
      }
    }
  }

  getUsuario( id: number ){
    for (let i = 0; i < this.listaUsuarios.length; i++) {
      if (this.listaUsuarios[i].IDUsuario == id) {
        return this.listaUsuarios[i].NombreUsuario + " " + this.listaUsuarios[i].ApellidoUsuario
      }
    }
  }

  getSucursal( id: number ){
    for (let i = 0; i < this.listaSucursales.length; i++) {
      if (this.listaSucursales[i].IDSucursal == id) {
        return this.listaSucursales[i].Sucursal
      }
    }
  }

  buscar( data: any) {
    const listab: any = [];
    if (this.data !== '') {
      for (let elemento of this.filtro){
        if (elemento.Producto.toLowerCase().indexOf(this.data.toLowerCase()) > -1 ) {
            listab.push(elemento);
          this.filtro = listab;
        }else {
          if (listab.length === 0) {
            this.filtro = [];
          }
        }
      }
    }else {
      this.filtro = [];
      this.getProductos();
    }
  }

  postComentarios(){
    const data = {
      Comentario: this.Comentario,
      Estrellas: 5,
      IDProducto: this.IDProducto,
      IDUsuario: this.IDUsuario
    };
    this.tiendaService.postComentarios(data).subscribe(
      (datos) => {
        this.tiendaService.getComentarios().subscribe(res => {
          this.listaComentarios = res.json();
          this.setData(this.IDProducto)
          this.Comentario = null
        });
      }
    );
  }

  deleteComentarios( id: number ){
    this.tiendaService.deleteComentarios(id).subscribe(
      (datos) => {
        this.tiendaService.getComentarios().subscribe(res => {
          this.listaComentarios = res.json();
          this.setData(this.IDProducto)
          this.Comentario = null
        });
      }
    );
  }

  mon(e){
    if(e.target.checked){
      sessionStorage.setItem('user', this.Usuario)
      sessionStorage.setItem('pass', this.Contrasena)
    }else{
      sessionStorage.clear()
    }
  }

}
