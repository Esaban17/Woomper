import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuariosService } from '../../../services/usuarios.service';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html'
})
export class UsuariosComponent implements OnInit {

  listaUsuarios = [];
  listaRoles = [];

  Token: string = null;

  IDUsuario: number;
  Imagen: string;
  NombreUsuario: string;
  ApellidoUsuario: string;
  DireccionUsuario: string;
  CorreoUsuario: string;
  TelefonoUsuario: string;
  Usuario: string;
  Contrasena: string;
  IDRol: number;

  constructor( private router: Router, private usuariosService: UsuariosService ) { }

  ngOnInit() {
    if (AppComponent.UL[0] == null) {
      this.router.navigate(['']);
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.getRoles();
        this.getUsuarios();
      }else{
        this.router.navigate(['']);
      }
    }
  }

  getUsuarios(){
    this.usuariosService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
    });
  }

  getRoles(){
    this.usuariosService.getRoles().subscribe(res => {
      this.listaRoles = res.json();
    });
  }

  postUsuarios(){
    const data = {
      NombreUsuario: this.NombreUsuario,
      ApellidoUsuario: this.ApellidoUsuario,
      DireccionUsuario: this.DireccionUsuario,
      CorreoUsuario: this.CorreoUsuario,
      TelefonoUsuario: this.TelefonoUsuario,
      Usuario: this.Usuario,
      Contrasena: this.Contrasena,
      IDRol: this.IDRol
    };
    this.usuariosService.postUsuarios(data).subscribe(
      (datos) => {
        this.getUsuarios();
      }
    );
  }

  putUsuarios(){
    const data = {
      NombreUsuario: this.NombreUsuario,
      ApellidoUsuario: this.ApellidoUsuario,
      DireccionUsuario: this.DireccionUsuario,
      CorreoUsuario: this.CorreoUsuario,
      TelefonoUsuario: this.TelefonoUsuario,
      Contrasena: this.Contrasena,
      IDRol: this.IDRol
    };
    this.usuariosService.putUsuarios(this.IDUsuario, data).subscribe(
      (datos) => {
        this.getUsuarios();
      }
    );
  }

  deleteUsuarios( id: any ){
    this.usuariosService.deleteUsuarios(id).subscribe(
      (datos) => {
        this.getUsuarios();
      }
    );
  }

  setData( item: any ){
    this.IDUsuario = item.IDUsuario
    this.NombreUsuario = item.NombreUsuario
    this.ApellidoUsuario = item.ApellidoUsuario
    this.DireccionUsuario = item.DireccionUsuario
    this.CorreoUsuario = item.CorreoUsuario
    this.TelefonoUsuario = item.TelefonoUsuario
    this.Contrasena = item.Contrasena
    this.IDRol = item.IDRol
  }

  limpiar(){
    this.IDUsuario = null
    this.NombreUsuario = null
    this.ApellidoUsuario = null
    this.DireccionUsuario = null
    this.CorreoUsuario = null
    this.TelefonoUsuario = null
    this.Contrasena = null
    this.IDRol = null
  }

  getRol( id: number ){
    for (let i = 0; i < this.listaRoles.length; i++) {
      if (this.listaRoles[i].IDRol == id) {
        return this.listaRoles[i].Rol
      }
    }
  }

  buscar( data: any ) {
    const listab: any = [];
    if (data !== '') {
      for (let elemento of this.listaUsuarios){
        if (elemento.NombreUsuario.toLowerCase().indexOf(data.toLowerCase()) > -1 ) {
            listab.push(elemento);
          this.listaUsuarios = listab;
        }else {
          if (listab.length === 0) {
            this.listaUsuarios = [];
          }
        }
      }
    }else {
      this.listaUsuarios = [];
      this.getUsuarios();
    }
  }

}
