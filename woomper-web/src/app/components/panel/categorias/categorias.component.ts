import { Component, OnInit } from '@angular/core';
import { CategoriasService } from '../../../services/categorias.service';
import { Router } from '@angular/router';
import { HeaderComponent } from '../../header/header.component';
import { LoginComponent } from '../../login/login.component';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html'
})
export class CategoriasComponent implements OnInit {

  listaCategorias = [];

  Acceso: boolean = false;
  Token: string = null;

  IDCategoria: number;
  Categoria: string;

  constructor( private router: Router, private categoriasService: CategoriasService ) { }

  ngOnInit() {
    if (AppComponent.UL[0] == null) {
      this.router.navigate(['']);
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.getCategorias();
      }else{
        this.router.navigate(['']);
      }
    }
  }

  getCategorias(){
    this.categoriasService.getCategorias().subscribe(res => {
      this.listaCategorias = res.json();
    });
  }

  postCategorias(){
    const data = {
      Categoria: this.Categoria
    };
    this.categoriasService.postCategorias(data).subscribe(
      (datos) => {
        this.getCategorias();
      }
    );
  }

  putCategorias(){
    const data = {
      Categoria: this.Categoria
    };
    this.categoriasService.putCategorias(this.IDCategoria, data).subscribe(
      (datos) => {
        this.getCategorias();
      }
    );
  }

  deleteCategorias( id: any ){
    this.categoriasService.deleteCategorias(id).subscribe(
      (datos) => {
        this.getCategorias();
      }
    );
  }

  setData( item: any ){
    this.IDCategoria = item.IDCategoria
    this.Categoria = item.Categoria
  }

  limpiar(){
    this.IDCategoria = null
    this.Categoria = null
  }

  buscar( data: any ) {
    const listab: any = [];
    if (data !== '') {
      for (let elemento of this.listaCategorias){
        if (elemento.Categoria.toLowerCase().indexOf(data.toLowerCase()) > -1 ) {
            listab.push(elemento);
          this.listaCategorias = listab;
        }else {
          if (listab.length === 0) {
            this.listaCategorias = [];
          }
        }
      }
    }else {
      this.listaCategorias = [];
      this.getCategorias();
    }
  }

}
