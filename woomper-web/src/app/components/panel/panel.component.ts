import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';
import { UsuariosService } from '../../services/usuarios.service';
import { SucursalesService } from '../../services/sucursales.service';
import { ProductosService } from '../../services/productos.service';
import { PedidosService } from '../../services/pedidos.service';
import { CategoriasService } from '../../services/categorias.service';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html'
})
export class PanelComponent implements OnInit {

  listaUsuarios = []
  listaSucursales = []
  listaProductos = []
  listaPedidos = []
  listaRoles = []
  listaCategorias = []
  listaProductosRecientes = []
  listaUsuariosRecientes = []
  Token: string = null;
  Usuarios: number;

  constructor( private router: Router, private categoriasService: CategoriasService, private usuariosService: UsuariosService, private sucursalesService: SucursalesService, private productosService: ProductosService, private pedidosService: PedidosService ) { }

  ngOnInit() {
    if (AppComponent.UL[0] == null) {
      this.router.navigate(['']);
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.getUsuarios()
        this.getSucursales()
        this.getProductos()
        this.getPedidos()
        this.getRoles()
        this.getCategorias()
      }else{
        this.router.navigate(['']);
      }
    }
  }

  getUsuarios(){
    this.usuariosService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
      for (let i = 1; i < 6; i++) {
        this.listaUsuariosRecientes.push(this.listaUsuarios[this.listaUsuarios.length - i])        
      }
    });
  }

  getSucursales(){
    this.sucursalesService.getSucursales().subscribe(res => {
      this.listaSucursales = res.json();
    });
  }

  getProductos(){
    this.productosService.getProductos().subscribe(res => {
      this.listaProductos = res.json();
      for (let i = 1; i < 6; i++) {
        this.listaProductosRecientes.push(this.listaProductos[this.listaProductos.length - i])
      }
    });
  }

  getPedidos(){
    this.pedidosService.getPedidos().subscribe(res => {
      this.listaPedidos = res.json();
    });
  }

  getRoles(){
    this.usuariosService.getRoles().subscribe(res => {
      this.listaRoles = res.json();
    });
  }

  getCategorias(){
    this.categoriasService.getCategorias().subscribe(res => {
      this.listaCategorias = res.json();
    });
  }

  getCategoria( id: number ){
    for (let i = 0; i < this.listaCategorias.length; i++) {
      if ( this.listaCategorias[i].IDCategoria == id ) {
        return this.listaCategorias[i].Categoria
      }
    }
  }

  getRol( id: number ){
    for (let i = 0; i < this.listaRoles.length; i++) {
      if ( this.listaRoles[i].IDRol == id ) {
        return this.listaRoles[i].Rol
      }
    }
  }

}
