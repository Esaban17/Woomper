import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {

  ID: number;

  constructor() { }

  ngOnInit() {
    this.ID = AppComponent.UL[0].Usuario.IDRol
  }

}
