import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../../../app.component';
import { PedidosService } from '../../../services/pedidos.service';
import { FacturasService } from '../../../services/facturas.service';
import { DetallefacturasService } from '../../../services/detallefacturas.service';
import { ProductosService } from '../../../services/productos.service';
import { EstadosService } from '../../../services/estados.service';
import { UsuariosService } from '../../../services/usuarios.service';
import { EmailService } from '../../../services/email.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html'
})
export class PedidosComponent implements OnInit {

  listaPedidos = [];
  filtroPedidos = [];
  listaUsuarios = [];
  listaEstados = [];
  listaFacturas = [];
  listaDetalleFacturas = [];
  listaProductos = [];
  filtroDetalleFacturas = [];

  NombreUsuario: string;
  ApellidoUsuario: string;
  CorreoUsuario: string;
  IDPedido: number;
  IDEstado: number;
  Porcentaje: string;
  Estado: string;
  IDUsuario: number;
  IDFactura: number;
  IDRol: number;
  Total: number;
  SubTotal: number;
  CodigoFactura: string;
  Numero: number;
  CodigoPedido: number;
  SinImagen: boolean;

  Acceso: boolean = false;
  Token: string = null;

  constructor( private emailService: EmailService, private usuariosService: UsuariosService, private estadosService: EstadosService, private pedidosService: PedidosService, private productosService: ProductosService, private router: Router, private facturasService: FacturasService, private detallefacturasService: DetallefacturasService ) { }

  ngOnInit() {
    if (AppComponent.UL[0] == null) {
      this.router.navigate(['']);
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.IDUsuario = AppComponent.UL[0].Usuario.IDUsuario
        this.IDRol = AppComponent.UL[0].Usuario.IDRol
        this.getPedidos();
        this.getFacturas();
        this.getDetalleFacturas();
        this.getProductos();
        this.getEstados();
        this.getUsuarios();
      }else{
        this.router.navigate(['']);
      }
    } 
  }

  getPedidos(){
    this.filtroPedidos = [];
    this.pedidosService.getPedidos().subscribe(res => {
      this.listaPedidos = res.json();
      if ( this.IDRol == 1 ) {
        this.filtroPedidos = this.listaPedidos
      }else{
        for (let i = 0; i < this.listaPedidos.length; i++) {
          if ( this.IDUsuario == this.listaPedidos[i].IDUsuario ) {
            this.filtroPedidos.push(this.listaPedidos[i])
          }
        }
      }
    });
  }

  getUsuarios(){
    this.usuariosService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
    });
  }

  deletePedido( id: number ){
    this.pedidosService.deletePedidos(id).subscribe(
      (datos) => {
        this.getPedidos();
      }
    );
  }

  Factura( id: number ){
    for (let z = 0; z < this.listaPedidos.length; z++) {
      if (id == this.listaPedidos[z].IDPedido) {
        this.IDEstado = this.listaPedidos[z].IDEstado
      }
    }
    this.SubTotal = 0;
    this.filtroDetalleFacturas = [];
    for (let i = 0; i < this.listaFacturas.length; i++) {
      if ( id == this.listaFacturas[i].IDPedido ) {
        this.IDFactura = this.listaFacturas[i].IDFactura
        this.IDPedido = this.listaFacturas[i].IDPedido
        this.CodigoFactura = this.listaFacturas[i].CodigoFactura
        this.Total = this.listaFacturas[i].Total
        for (let u = 0; u < this.listaDetalleFacturas.length; u++) {
          if ( this.IDFactura ==  this.listaDetalleFacturas[u].IDFactura ) {
            this.filtroDetalleFacturas.push(this.listaDetalleFacturas[u]);
            this.SubTotal = this.SubTotal +  this.listaDetalleFacturas[u].SubTotal;
          }
        }
      }
    }
  }

  cambiarEstado(){
    const data = {
      CodigoPedido: this.CodigoPedido,
      IDEstado: this.IDEstado,
      IDUsuario: this.IDUsuario
    };
    this.pedidosService.putPedidos(this.IDPedido, data).subscribe(
      (datos) => {
        this.getPedidos();
        for (let i = 0; i < this.listaUsuarios.length; i++) {
          if (this.IDUsuario == this.listaUsuarios[i].IDUsuario) {
            this.NombreUsuario = this.listaUsuarios[i].NombreUsuario
            this.ApellidoUsuario = this.listaUsuarios[i].ApellidoUsuario
            this.CorreoUsuario = this.listaUsuarios[i].CorreoUsuario
          }
        }
        for (let i = 0; i < this.listaEstados.length; i++) {
          if (this.IDEstado == this.listaEstados[i].IDEstado) {
            this.Estado = this.listaEstados[i].Estado
            this.Porcentaje = this.listaEstados[i].Porcentaje
          }
        }
        const data = {
          NombreUsuario: this.NombreUsuario,
          ApellidoUsuario: this.ApellidoUsuario,
          CorreoUsuario: this.CorreoUsuario,
          CodigoPedido: this.CodigoPedido,
          Estado: this.Estado,
          Porcentaje: this.Porcentaje
        }  
        this.emailService.emailPedido(data).subscribe(
          (datos) => {
            this.getPedidos();
        });
      }
    );
  }

  setData( item: any){
    this.IDPedido = item.IDPedido
    this.CodigoPedido = item.CodigoPedido
    this.IDEstado = item.IDEstado
    this.IDUsuario = item.IDUsuario
  }

  refresh(){
    this.filtroPedidos = []
    this.getPedidos();
  }

  getFacturas(){
    this.facturasService.getFacturas().subscribe(res => {
      this.listaFacturas = res.json();
    });
  }

  getDetalleFacturas(){
    this.detallefacturasService.getDetalleFacturas().subscribe(res => {
      this.listaDetalleFacturas = res.json();
    });
  }

  getProductos(){
    this.productosService.getProductos().subscribe(res => {
      this.listaProductos = res.json();
    });
  }

  getEstados(){
    this.estadosService.getEstados().subscribe(res => {
      this.listaEstados = res.json();
    });
  }

  getProducto( id: number ){
    for (let i = 0; i < this.listaProductos.length; i++) {
      if (this.listaProductos[i].IDProducto == id) {
        return this.listaProductos[i].Producto
      }
    }
  }

  getPrecio( id: number ){
    for (let i = 0; i < this.listaProductos.length; i++) {
      if (this.listaProductos[i].IDProducto == id) {
        return this.listaProductos[i].Precio
      }
    }
  }

  getEstado( id: number ){
    for (let i = 0; i < this.listaEstados.length; i++) {
      if (this.listaEstados[i].IDEstado == id) {
        return this.listaEstados[i].Estado
      }
    }
  }

  getPorcentaje( id: number ){
    for (let i = 0; i < this.listaEstados.length; i++) {
      if (this.listaEstados[i].IDEstado == id) {
        this.Porcentaje = this.listaEstados[i].Porcentaje+"%"
        return this.listaEstados[i].Porcentaje
      }
    }
  }

  getUsuario( id: number ){
    for (let i = 0; i < this.listaUsuarios.length; i++) {
      if (this.listaUsuarios[i].IDUsuario == id) {
        return this.listaUsuarios[i].NombreUsuario
      }
    }
  }

  buscar( data: any ) {
    const listab: any = [];
    if (data !== '') {
      for (let elemento of this.filtroPedidos){
        if (elemento.CodigoPedido.toLowerCase().indexOf(data.toLowerCase()) > -1 ) {
            listab.push(elemento);
          this.filtroPedidos = listab;
        }else {
          if (listab.length === 0) {
            this.filtroPedidos = [];
          }
        }
      }
    }else {
      this.filtroPedidos = [];
      this.getPedidos();
    }
  }

  getImagen( id: number){
    if (id == null) {
      this.SinImagen = true
    }else{
      this.SinImagen = false
      for (let i = 0; i < this.listaProductos.length; i++) {
        if ( this.listaProductos[i].IDProducto == id ) {
          return this.listaProductos[i].Imagen
        }
      }
    }
  }

}
