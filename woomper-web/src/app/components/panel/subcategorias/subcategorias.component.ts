import { Component, OnInit } from '@angular/core';
import { SubcategoriasService } from '../../../services/subcategorias.service';
import { CategoriasService } from '../../../services/categorias.service';
import { Router } from '@angular/router';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-subcategorias',
  templateUrl: './subcategorias.component.html'
})
export class SubcategoriasComponent implements OnInit {

  listaSubCategorias = [];
  listaCategorias = [];

  Acceso: boolean = false;
  Token: string = null;

  IDSubCategoria: number;
  SubCategoria: string;
  IDCategoria: number;

  constructor( private router: Router, private subcategoriasService: SubcategoriasService, private categoriasService: CategoriasService ) { }

  ngOnInit() {
    if (AppComponent.UL[0] == null) {
      this.router.navigate(['']);
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.getSubCategorias();
        this.getCategorias();
      }else{
        this.router.navigate(['']);
      }
    }
  }

  getSubCategorias(){
    this.subcategoriasService.getSubCategorias().subscribe(res => {
      this.listaSubCategorias = res.json();
    });
  }

  getCategorias(){
    this.categoriasService.getCategorias().subscribe(res => {
      this.listaCategorias = res.json();
    });
  }

  postSubCategorias(){
    const data = {
      SubCategoria: this.SubCategoria,
      IDCategoria: this.IDCategoria
    };
    this.subcategoriasService.postSubCategorias(data).subscribe(
      (datos) => {
        this.getSubCategorias();
      }
    );
  }

  putSubCategorias(){
    const data = {
      SubCategoria: this.SubCategoria,
      IDCategoria: this.IDCategoria
    };
    this.subcategoriasService.putSubCategorias(this.IDSubCategoria, data).subscribe(
      (datos) => {
        this.getSubCategorias();
      }
    );
  }

  deleteSubCategorias( id: any ){
    this.subcategoriasService.deleteSubCategorias(id).subscribe(
      (datos) => {
        this.getSubCategorias();
      }
    );
  }

  setData( item: any ){
    this.IDSubCategoria = item.IDSubCategoria
    this.SubCategoria = item.SubCategoria
    this.IDCategoria = item.IDCategoria
  }

  limpiar(){
    this.IDSubCategoria = null
    this.SubCategoria = null
    this.IDCategoria = null
  }

  buscar( data: any ) {
    const listab: any = [];
    if (data !== '') {
      for (let elemento of this.listaSubCategorias){
        if (elemento.SubCategoria.toLowerCase().indexOf(data.toLowerCase()) > -1 ) {
            listab.push(elemento);
          this.listaSubCategorias = listab;
        }else {
          if (listab.length === 0) {
            this.listaSubCategorias = [];
          }
        }
      }
    }else {
      this.listaSubCategorias = [];
      this.getSubCategorias();
    }
  }
  
}
