import { Component, OnInit } from '@angular/core';
import { SucursalesService } from '../../../services/sucursales.service';
import { Router } from '@angular/router';
import { LoginComponent } from '../../login/login.component';
import { HeaderComponent } from '../../header/header.component';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-sucursales',
  templateUrl: './sucursales.component.html'
})
export class SucursalesComponent implements OnInit {

  listaSucursales = [];

  Acceso: boolean = false;
  Token: string = null;

  IDSucursal: number;
  Sucursal: string;
  Direccion: string;
  Telefono: string;
  Ubicacion: string;

  constructor( private router: Router, private sucursalesService: SucursalesService ) { }

  ngOnInit() {
    if (AppComponent.UL[0] == null) {
      this.router.navigate(['']);
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.getSucursales();
      }else{
        this.router.navigate(['']);
      }
    }
  }

  getSucursales(){
    this.sucursalesService.getSucursales().subscribe(res => {
      this.listaSucursales = res.json();
    });
  }

  postSucursales(){
    const data = {
      Sucursal: this.Sucursal,
      Direccion: this.Direccion,
      Telefono: this.Telefono,
      Ubicacion: this.Ubicacion
    };
    this.sucursalesService.postSucursales(data).subscribe(
      (datos) => {
        this.getSucursales();
      }
    );
  }

  putSucursales(){
    const data = {
      Sucursal: this.Sucursal,
      Direccion: this.Direccion,
      Telefono: this.Telefono,
      Ubicacion: this.Ubicacion
    };
    this.sucursalesService.putSucursales(this.IDSucursal, data).subscribe(
      (datos) => {
        this.getSucursales();
      }
    );
  }

  deleteSucursales( id: any ){
    this.sucursalesService.deleteSucursales(id).subscribe(
      (datos) => {
        this.getSucursales();
      }
    );
  }

  setData( item: any ){
    this.IDSucursal = item.IDSucursal
    this.Sucursal = item.Sucursal
    this.Direccion = item.Direccion
    this.Telefono = item.Telefono
    this.Ubicacion = item.Ubicacion
  }

  limpiar(){
    this.IDSucursal = null
    this.Sucursal = null
    this.Direccion = null
    this.Telefono = null
    this.Ubicacion = null
  }

  buscar( data: any ) {
    const listab: any = [];
    if (data !== '') {
      for (let elemento of this.listaSucursales){
        if (elemento.Sucursal.toLowerCase().indexOf(data.toLowerCase()) > -1 ) {
            listab.push(elemento);
          this.listaSucursales = listab;
        }else {
          if (listab.length === 0) {
            this.listaSucursales = [];
          }
        }
      }
    }else {
      this.listaSucursales = [];
      this.getSucursales();
    }
  }

}
