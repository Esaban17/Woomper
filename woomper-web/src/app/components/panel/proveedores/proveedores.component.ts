import { Component, OnInit } from '@angular/core';
import { ProveedoresService } from '../../../services/proveedores.service';
import { LoginComponent } from '../../login/login.component';
import { Router } from '@angular/router';
import { HeaderComponent } from '../../header/header.component';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html'
})
export class ProveedoresComponent implements OnInit {

  listaProveedores = [];
  
  Acceso: boolean = false;
  Token: string = null;

  IDProveedor: number;
  Proveedor: string;
  Direccion: string;
  Telefono: string;

  constructor( private router: Router, private proveedoresService: ProveedoresService ) { }

  ngOnInit() {
    if (AppComponent.UL[0] == null) {
      this.router.navigate(['']);
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.getProveedores();
      }else{
        this.router.navigate(['']);
      }
    }
  }

  getProveedores(){
    this.proveedoresService.getProveedores().subscribe(res => {
      this.listaProveedores = res.json();
    });
  }

  postProveedores(){
    const data = {
      Proveedor: this.Proveedor,
      Direccion: this.Direccion,
      Telefono: this.Telefono
    };
    this.proveedoresService.postProveedores(data).subscribe(
      (datos) => {
        this.getProveedores();
      }
    );
  }

  putProveedores(){
    const data = {
      Proveedor: this.Proveedor,
      Direccion: this.Direccion,
      Telefono: this.Telefono
    };
    this.proveedoresService.putProveedores(this.IDProveedor, data).subscribe(
      (datos) => {
        this.getProveedores();
      }
    );
  }

  deleteProveedores( id: any ){
    this.proveedoresService.deleteProveedores(id).subscribe(
      (datos) => {
        this.getProveedores();
      }
    );
  }

  setData( item: any ){
    this.IDProveedor = item.IDProveedor
    this.Proveedor = item.Proveedor
    this.Direccion = item.Direccion
    this.Telefono = item.Telefono
  }

  limpiar(){
    this.IDProveedor = null
    this.Proveedor = null
    this.Direccion = null
    this.Telefono = null
  }

  buscar( data: any ) {
    const listab: any = [];
    if (data !== '') {
      for (let elemento of this.listaProveedores){
        if (elemento.Proveedor.toLowerCase().indexOf(data.toLowerCase()) > -1 ) {
            listab.push(elemento);
          this.listaProveedores = listab;
        }else {
          if (listab.length === 0) {
            this.listaProveedores = [];
          }
        }
      }
    }else {
      this.listaProveedores = [];
      this.getProveedores();
    }
  }

}
