import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../../../services/productos.service';
import { CategoriasService } from '../../../services/categorias.service';
import { SubcategoriasService } from '../../../services/subcategorias.service';
import { ProveedoresService } from '../../../services/proveedores.service';
import { SucursalesService } from '../../../services/sucursales.service';
import { Router } from '@angular/router';
import { LoginComponent } from '../../login/login.component';
import { HeaderComponent } from '../../header/header.component';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html'
})
export class ProductosComponent implements OnInit {

  listaProductos = [];
  listaCategorias = [];
  listaSubCategorias = [];
  listaProveedores = [];
  listaSucursales = [];

  filtroSubCategorias = [];

  Disponible: string = "Disponible";
  Agotado: string = "Agotado";
  Acceso: boolean = false;
  Token: string = null;

  IDProducto: number;
  Imagen: string;
  Producto: string;
  Precio: number;
  Cantidad: number;
  Fecha: string;
  Estado: string;
  IDCategoria: number;
  IDSubCategoria: number;
  IDProveedor: number;
  IDSucursal: number;

  constructor( private router: Router, private productosService: ProductosService, private categoriasService: CategoriasService, private subcategoriasService: SubcategoriasService, private proveedoresService: ProveedoresService, private sucursalesService: SucursalesService ) { }

  ngOnInit() {
    if (AppComponent.UL[0] == null) {
      this.router.navigate(['']);
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.getProductos();
        this.getCategorias();
        this.getSubCategorias();
        this.getProveedores();
        this.getSucursales();
      }else{
        this.router.navigate(['']);
      }
    }
  }

  getProductos(){
    this.productosService.getProductos().subscribe(res => {
      this.listaProductos = res.json();
    });
  }

  getCategorias(){
    this.categoriasService.getCategorias().subscribe(res => {
      this.listaCategorias = res.json();
    });
  }

  getSubCategorias(){
    this.subcategoriasService.getSubCategorias().subscribe(res => {
      this.listaSubCategorias = res.json();
    });
  }

  getProveedores(){
    this.proveedoresService.getProveedores().subscribe(res => {
      this.listaProveedores = res.json();
    });
  }

  getSucursales(){
    this.sucursalesService.getSucursales().subscribe(res => {
      this.listaSucursales = res.json();
    });
  }

  postProductos(){
    if (this.Cantidad == 0) {
      this.Estado = "Agotado";
    }else{
      this.Estado = "Disponible";
    }
    const data = {
      Producto: this.Producto,
      Precio: this.Precio,
      Cantidad: this.Cantidad,
      Fecha: new Date().toLocaleDateString(),
      Estado: this.Estado,
      IDCategoria: this.IDCategoria,
      IDSubCategoria: this.IDSubCategoria,
      IDProveedor: this.IDProveedor,
      IDSucursal: this.IDSucursal
    };
    this.productosService.postProductos(data).subscribe(
      (datos) => {
        this.productosService.getProductos().subscribe(res => {
          this.listaProductos = res.json();
          for (let i = 0; i < this.listaProductos.length; i++) {
            this.IDProducto = this.listaProductos[i].IDProducto
            console.log(this.IDProducto);
          }
        });
      }
    );
  }

  putProductos(){
    if (this.Cantidad == 0) {
      this.Estado = "Agotado";
    }else{
      this.Estado = "Disponible";
    }
    const data = {
      Producto: this.Producto,
      Precio: this.Precio,
      Cantidad: this.Cantidad,
      Estado: this.Estado,
      IDCategoria: this.IDCategoria,
      IDSubCategoria: this.IDSubCategoria,
      IDProveedor: this.IDProveedor,
      IDSucursal: this.IDSucursal
    };
    this.productosService.putProductos(this.IDProducto, data).subscribe(
      (datos) => {
        this.getProductos();
      }
    );
  }

  deleteProductos( id: any ){
    this.productosService.deleteProductos(id).subscribe(
      (datos) => {
        this.getProductos();
      }
    );
  }

  setData( item: any ){
    this.IDProducto = item.IDProducto
    this.Producto = item.Producto
    this.Precio = item.Precio
    this.Cantidad = item.Cantidad
    this.Fecha = item.Fecha
    this.Estado = item.Estado
    this.IDCategoria = item.IDCategoria
    this.IDSubCategoria = item.IDSubCategoria
    this.IDProveedor = item.IDProveedor
    this.IDSucursal = item.IDSucursal
  }

  limpiar(){
    this.IDProducto = null
    this.Producto = null
    this.Precio = null
    this.Cantidad = null
    this.Fecha = null
    this.Estado = null
    this.IDCategoria = null
    this.IDSubCategoria = null
    this.IDProveedor = null
    this.IDSucursal = null
  }

  filtrarSubCategorias( id: number ){
    this.filtroSubCategorias = [];
    for (let i = 0; i < this.listaSubCategorias.length; i++) {
      if (this.listaSubCategorias[i].IDCategoria == id) {
        this.filtroSubCategorias.push(this.listaSubCategorias[i])
      }
    }
  }

  getCategoria( id: number ){

    for (let i = 0; i < this.listaCategorias.length; i++) {
      if (this.listaCategorias[i].IDCategoria == id) {
        return this.listaCategorias[i].Categoria
      }
    }
  }

  getSubCategoria( id: number ){
    for (let i = 0; i < this.listaSubCategorias.length; i++) {
      if (this.listaSubCategorias[i].IDSubCategoria == id) {
        return this.listaSubCategorias[i].SubCategoria
      }
    }
  }

  getProveedor( id: number ){
    for (let i = 0; i < this.listaProveedores.length; i++) {
      if (this.listaProveedores[i].IDProveedor == id) {
        return this.listaProveedores[i].Proveedor
      }
    }
  }

  getSucursal( id: number ){
    for (let i = 0; i < this.listaSucursales.length; i++) {
      if (this.listaSucursales[i].IDSucursal == id) {
        return this.listaSucursales[i].Sucursal
      }
    }
  }

  public subirPortada(IDProducto: number,$event) {
    if ($event.target.files.length === 1) {
        this.productosService.subirPortada(IDProducto, $event.target.files[0]).subscribe(response => {
          this.getProductos();
        },  
        error => {
            console.error(error);
        });
    }
  }

  buscar( data: any ) {
    const listab: any = [];
    if (data !== '') {
      for (let elemento of this.listaProductos){
        if (elemento.Producto.toLowerCase().indexOf(data.toLowerCase()) > -1 ) {
            listab.push(elemento);
          this.listaProductos = listab;
        }else {
          if (listab.length === 0) {
            this.listaProductos = [];
          }
        }
      }
    }else {
      this.listaProductos = [];
      this.getProductos();
    }
  }

}
