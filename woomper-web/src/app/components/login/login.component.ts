import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from '../../services/autenticacion.service';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';
import { VerificarService } from '../../services/verificar.service';
import { EmailService } from '../../services/email.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  
  Usuario: string;
  Contrasena: string;
  NombreUsuario: string;
  CorreoUsuario: string;
  ApellidoUsuario: string
  Imagen: string
  Codigo: string;

  listaUsuarios = [];

  static Hora: string
  
  Existe: boolean;
  Vacio: boolean;
  Fallo: boolean;
  Verificar: boolean;
  Olvide: boolean;
  Cambiar: boolean
  Encontrado: boolean

  cod: string
  user: string
  pass: string
  name: string
  lastname: string
  dire: string
  tel: string
  mail: string
  rol: number
  status: string
  id: number

  constructor( private autenticacionService: AutenticacionService, private router: Router, private verificarService: VerificarService, private emailService: EmailService ) { }

  ngOnInit() {
    this.getUsuarios();
  }

  login(){ 
    this.Encontrado = undefined
    this.Cambiar = undefined
    this.Verificar = undefined
    this.Olvide = undefined
    if ( this.Usuario == null || this.Contrasena == null || this.Usuario == "" || this.Contrasena == "" ) {
      this.Vacio = true;
      this.Fallo = undefined;
    }else{
      this.Vacio = false;
      this.autenticacionService.login({"Usuario":this.Usuario, "Contrasena":this.Contrasena}).subscribe(
        (data) => {          
          if (JSON.parse(data._body).msg === "No Encontrado") {
            this.Fallo = true;   
            sessionStorage.clear()      
          } else {
            AppComponent.UL[0] = JSON.parse(data._body);
            this.NombreUsuario = AppComponent.UL[0].Usuario.NombreUsuario
            this.id = AppComponent.UL[0].Usuario.IDUsuario
            this.user = AppComponent.UL[0].Usuario.Usuario
            this.pass = AppComponent.UL[0].Usuario.Contrasena
            this.cod = AppComponent.UL[0].Usuario.Codigo
            this.name = AppComponent.UL[0].Usuario.NombreUsuario
            this.lastname = AppComponent.UL[0].Usuario.ApellidoUsuario
            this.dire = AppComponent.UL[0].Usuario.DireccionUsuario
            this.tel = AppComponent.UL[0].Usuario.TelefonoUsuario
            this.mail = AppComponent.UL[0].Usuario.CorreoUsuario
            this.rol = AppComponent.UL[0].Usuario.IDRol
            if (AppComponent.UL[0].Usuario.Estado == 0) {
              this.Verificar = true
              AppComponent.UL[0] = null
              sessionStorage.clear();
            } else{
              LoginComponent.Hora = new Date().toLocaleTimeString();
              this.Fallo = false;
              this.Verificar = undefined
            }
          }
        }
      );
    }
  }

  getUsuarios(){
    this.autenticacionService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
    });
  }

  verificar(){
    if ( this.cod == this.Codigo) {
      const data = {
        NombreUsuario: this.name,
        ApellidoUsuario: this.lastname,
        DireccionUsuario: this.dire,
        CorreoUsuario: this.mail,
        TelefonoUsuario:this.tel,
        Contrasena: this.pass,
        Estado: 1,
        Codigo: this.cod,
        IDRol: this.rol
      };
      this.verificarService.putUsuarios(this.id, data).subscribe(
        (datos) => {
          this.Verificar = undefined
          this.Fallo = false
          this.login()
        }
      ); 
    }else{
      this.Verificar = false
      setTimeout( () => { 
        this.Verificar = true
      }, 3000 );
    }
  }

  buscarU(){
    for (let i = 0; i < this.listaUsuarios.length; i++) {
      if (this.Usuario == this.listaUsuarios[i].Usuario){
        this.Imagen = this.listaUsuarios[i].Imagen
        this.NombreUsuario = this.listaUsuarios[i].NombreUsuario
        this.ApellidoUsuario = this.listaUsuarios[i].ApellidoUsuario
        this.CorreoUsuario = this.listaUsuarios[i].CorreoUsuario
        this.cod = Math.floor((Math.random() * 9999) + 1000).toString()
        this.Encontrado = true
        this.Olvide = undefined
        const data = {
          Estado: 1,
          Contrasena: this.listaUsuarios[i].Contrasena,
          Codigo: this.cod
        };
        this.autenticacionService.cambiar(this.listaUsuarios[i].IDUsuario, data).subscribe(
          (datos) => {
            const data = {
              NombreUsuario: this.NombreUsuario,
              ApellidoUsuario: this.ApellidoUsuario,
              CorreoUsuario: this.CorreoUsuario,
              Codigo: this.cod
            };
            this.emailService.emailVerificar(data).subscribe(
              (datos) => {

              }
            );
          }
        );
        break
      }
    }
    if (this.Olvide != undefined) {
      this.Olvide = undefined
      this.Encontrado = false
      setTimeout( () => { 
        this.Olvide = true
        this.Encontrado = undefined
      }, 3000 ); 
    }
  }

  olvideC(){
    this.Olvide = true
    this.Encontrado = undefined
    this.Cambiar = undefined
    this.Verificar = undefined
  }

  verificar2(){
    if ( this.cod == this.Codigo) {
      this.Encontrado = undefined
      this.Cambiar = true
    }else{
      this.Verificar = false
      this.Encontrado = undefined
      setTimeout( () => { 
        this.Verificar = undefined
        this.Encontrado = true
      }, 3000 );
    }
  }

  nuevaC(){
    this.autenticacionService.login2({"Usuario":this.Usuario, "Codigo":this.Codigo}).subscribe(
      (data) => {          
        if (JSON.parse(data._body).msg === "No Encontrado") {
          this.Fallo = true;   
          sessionStorage.clear()      
        } else {
          AppComponent.UL[0] = JSON.parse(data._body);
          console.log(AppComponent.UL[0]);      
          this.NombreUsuario = AppComponent.UL[0].Usuario.NombreUsuario
          this.id = AppComponent.UL[0].Usuario.IDUsuario
          this.user = AppComponent.UL[0].Usuario.Usuario
          this.pass = AppComponent.UL[0].Usuario.Contrasena
          this.cod = AppComponent.UL[0].Usuario.Codigo
          this.name = AppComponent.UL[0].Usuario.NombreUsuario
          this.lastname = AppComponent.UL[0].Usuario.ApellidoUsuario
          this.dire = AppComponent.UL[0].Usuario.DireccionUsuario
          this.tel = AppComponent.UL[0].Usuario.TelefonoUsuario
          this.mail = AppComponent.UL[0].Usuario.CorreoUsuario
          this.rol = AppComponent.UL[0].Usuario.IDRol
          if (AppComponent.UL[0].Usuario.Estado == 0) {
            this.Verificar = true
            AppComponent.UL[0] = null
            sessionStorage.clear();
          } else{
            LoginComponent.Hora = new Date().toLocaleTimeString();
            this.Fallo = false;
            this.Verificar = undefined
          }
          const data2 = {
            Codigo: this.Codigo,
            Contrasena: this.Contrasena,
            Estado: 1
          };
          this.autenticacionService.cambiar(AppComponent.UL[0].Usuario.IDUsuario, data2).subscribe(
            (datos) => {
              AppComponent.UL[0].Usuario.Contrasena = this.Contrasena
              this.Cambiar = undefined
            }
          );
        }
      }
    );
  }

  mon(e){
    if(e.target.checked){
      sessionStorage.setItem('user', this.Usuario)
      sessionStorage.setItem('pass', this.Contrasena)
    }else{
      sessionStorage.clear()
    }
  }

}
