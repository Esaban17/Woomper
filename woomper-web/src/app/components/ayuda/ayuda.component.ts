import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ayuda',
  templateUrl: './ayuda.component.html'
})
export class AyudaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    
  }

  data: string
  filtro = []

  buscar(  ) {
    const listab: any = [];
    if (this.data !== '') {
      for (let elemento of this.filtro){
        if (elemento.Producto.toLowerCase().indexOf(this.data.toLowerCase()) > -1 ) {
            listab.push(elemento);
          this.filtro = listab;
        }else {
          if (listab.length === 0) {
            this.filtro = [];
          }
        }
      }
    }else {
      this.filtro = [];
    }
  }

}
