import { Component, OnInit } from '@angular/core';
import { EmailService } from '../../services/email.service';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html'
})
export class ContactoComponent implements OnInit {

  NombreUsuario: string
  ApellidoUsuario: string
  CorreoUsuario: string
  Mensaje: string
  Fallo: boolean
  Vacio: boolean

  constructor( private emailService: EmailService ) { }

  ngOnInit() {

  }

  postEmail(){
    if (this.NombreUsuario == null || this.ApellidoUsuario == null || this.CorreoUsuario == null || this.Mensaje == null || this.NombreUsuario == "" || this.ApellidoUsuario == "" || this.CorreoUsuario == "" || this.Mensaje == "" ) {
      this.Vacio = true
    }else{
      this.Vacio = undefined
      this.Fallo = false
      const data = {
        NombreUsuario: this.NombreUsuario,
        ApellidoUsuario: this.ApellidoUsuario,
        CorreoUsuario: this.CorreoUsuario,
        Mensaje: this.Mensaje
      };
      this.emailService.emailContacto(data).subscribe(
        (datos) => {
          this.emailService.emailContacto2(data).subscribe(
            (datos) => {
              this.NombreUsuario = null
              this.ApellidoUsuario = null
              this.CorreoUsuario = null
              this.Mensaje = null
            }
          ); 
        }
      ); 
    }
  }

}
