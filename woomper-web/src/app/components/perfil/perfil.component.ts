import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuariosService } from '../../services/usuarios.service';
import { HeaderComponent } from '../header/header.component';
import { AppComponent } from '../../app.component';
import { PedidosService } from '../../services/pedidos.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html'
})
export class PerfilComponent implements OnInit {

  listaUsuarios = [];
  listaPedidos = [];
  listaPedidosFiltro = [];

  Token: string = null;
  Name: string = null;
  LastName: string = null;

  IDUsuario: number;
  Imagen: string;
  NombreUsuario: string;
  ApellidoUsuario: string;
  DireccionUsuario: string;
  CorreoUsuario: string;
  TelefonoUsuario: string;
  Usuario: string;
  Contrasena: string;
  IDRol: number;

  ContrasenaA: string;
  ContrasenaN: string;
  Correcta: boolean;
  Vacio: boolean;

  Hora: string;

  Image: string;

  constructor( private router: Router, private usuariosService: UsuariosService, private pedidosService: PedidosService ) { }

  ngOnInit() {
    if (AppComponent.UL[0] == null) {
      this.router.navigate(['inicio']);
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.IDUsuario = AppComponent.UL[0].Usuario.IDUsuario
        this.Name = AppComponent.UL[0].Usuario.NombreUsuario
        this.LastName = AppComponent.UL[0].Usuario.ApellidoUsuario
        this.Image = AppComponent.UL[0].Usuario.Imagen
        this.getUsuarios();
        this.getPedidos();
        this.Hora = LoginComponent.Hora
      }else{
        this.router.navigate(['']);
      }
    }
  }

  getPedidos(){
    this.pedidosService.getPedidos().subscribe(res => {
      this.listaPedidos = res.json();
      for (let i = 0; i < this.listaPedidos.length; i++) {
        if (this.listaPedidos[i].IDUsuario == this.IDUsuario) {
          this.listaPedidosFiltro.push(this.listaPedidos[i])
        }
      }
    });
  }

  getUsuarios(){
    this.usuariosService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
    });
  }

  editarPerfil(){
    const data = {
      NombreUsuario: this.NombreUsuario,
      ApellidoUsuario: this.ApellidoUsuario,
      DireccionUsuario: this.DireccionUsuario,
      CorreoUsuario: this.CorreoUsuario,
      TelefonoUsuario: this.TelefonoUsuario,
      Contrasena: this.Contrasena,
      IDRol: this.IDRol
    };
    this.usuariosService.putUsuarios(this.IDUsuario, data).subscribe(
      (datos) => {
        AppComponent.UL[0].Usuario.NombreUsuario = this.NombreUsuario
        AppComponent.UL[0].Usuario.ApellidoUsuario = this.ApellidoUsuario
        AppComponent.UL[0].Usuario.DireccionUsuario = this.DireccionUsuario
        AppComponent.UL[0].Usuario.CorreoUsuario = this.CorreoUsuario
        AppComponent.UL[0].Usuario.TelefonoUsuario = this.TelefonoUsuario
        AppComponent.UL[0].Usuario.Contrasena = this.Contrasena
        AppComponent.UL[0].Usuario.IDRol = this.IDRol
        this.Name = this.NombreUsuario
        this.LastName = this.ApellidoUsuario
        this.getUsuarios();
      }
    );
  }

  eliminarPerfil(){
    this.usuariosService.deleteUsuarios(this.IDUsuario).subscribe(
      (datos) => {
        this.getUsuarios();
        AppComponent.UL[0] = null
        sessionStorage.clear();
      }
    );
  }

  public subirPortada(IDUsuario: number,$event) {
    if ($event.target.files.length === 1) {
        this.usuariosService.subirPortada(IDUsuario, $event.target.files[0]).subscribe(response => {
          this.usuariosService.getUsuarios().subscribe(res => {
            this.listaUsuarios = res.json();
            for (let i = 0; i < this.listaUsuarios.length; i++) {
              if ( this.IDUsuario == this.listaUsuarios[i].IDUsuario ) {
                this.Imagen = this.listaUsuarios[i].Imagen
              }
            }
            AppComponent.UL[0].Usuario.Imagen = this.Imagen
            this.Image = this.Imagen
            this.getUsuarios();
          });
        },  
        error => {
            console.error(error);
        });
    }
  }

  editarContrasena(){
    if (this.ContrasenaA == null || this.ContrasenaN == null || this.ContrasenaA == "" || this.ContrasenaN == "") {
      this.Vacio = true
    }else{
      this.Vacio = false
      if (this.ContrasenaA == AppComponent.UL[0].Usuario.Contrasena) {
        this.Correcta = true
        for (let i = 0; i < this.listaUsuarios.length; i++) {
          if (this.listaUsuarios[i].IDUsuario == AppComponent.UL[0].Usuario.IDUsuario) {
            const data = {
              NombreUsuario: AppComponent.UL[0].Usuario.NombreUsuario,
              ApellidoUsuario: AppComponent.UL[0].Usuario.ApellidoUsuario,
              DireccionUsuario: AppComponent.UL[0].Usuario.DireccionUsuario,
              CorreoUsuario: AppComponent.UL[0].Usuario.CorreoUsuario,
              TelefonoUsuario: AppComponent.UL[0].Usuario.TelefonoUsuario,
              Contrasena: this.ContrasenaN,
              IDRol: AppComponent.UL[0].Usuario.IDRol
            };
            this.usuariosService.putUsuarios(this.listaUsuarios[i].IDUsuario, data).subscribe(
              (datos) => {
                AppComponent.UL[0].Usuario.Contrasena = this.ContrasenaN
                this.getUsuarios();
              }
            );
          } 
        }    
      }else{
        this.Correcta = false
      }
    }
  }

  setData(){
    this.IDUsuario = AppComponent.UL[0].Usuario.IDUsuario 
    for (let i = 0; i < this.listaUsuarios.length; i++) {
      if ( this.IDUsuario == this.listaUsuarios[i].IDUsuario ) {
        this.Imagen = this.listaUsuarios[i].Imagen
        this.NombreUsuario = this.listaUsuarios[i].NombreUsuario
        this.ApellidoUsuario = this.listaUsuarios[i].ApellidoUsuario
        this.DireccionUsuario = this.listaUsuarios[i].DireccionUsuario
        this.CorreoUsuario = this.listaUsuarios[i].CorreoUsuario
        this.TelefonoUsuario = this.listaUsuarios[i].TelefonoUsuario
        this.Usuario = this.listaUsuarios[i].Usuario
        this.Contrasena = this.listaUsuarios[i].Contrasena
        this.IDRol = this.listaUsuarios[i].IDRol
      }
    }
  }

  limpiar(){
    this.IDUsuario = null
    this.NombreUsuario = null
    this.ApellidoUsuario = null
    this.CorreoUsuario = null
    this.Usuario = null
    this.Contrasena = null
    this.ContrasenaA = null
    this.ContrasenaN = null
  }

}
