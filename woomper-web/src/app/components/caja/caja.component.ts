import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../../services/usuarios.service';
import { AppComponent } from '../../app.component';
import { TiendaComponent } from '../tienda/tienda.component';
import { FacturasService } from '../../services/facturas.service';
import { DetallefacturasService } from '../../services/detallefacturas.service';
import { ProductosService } from '../../services/productos.service';
import { PedidosService } from '../../services/pedidos.service';
import { EmailService } from '../../services/email.service';
import { EstadosService } from '../../services/estados.service';

@Component({
  selector: 'app-caja',
  templateUrl: './caja.component.html'
})
export class CajaComponent implements OnInit {

  listaUsuarios = [];
  listaFacturas = [];
  listaProductos = [];
  listaPedidos = [];
  listaEstados = [];

  carrito = [];
  SubTotal: number;
  Total: number;
  IDPedido: number;
  IDEstado: number;
  Estado: string;
  Porcentaje: number;
  Comprado: boolean;

  IDUsuario: number;
  Imagen: string;
  NombreUsuario: string;
  ApellidoUsuario: string;
  DireccionUsuario: string;
  CorreoUsuario: string;
  TelefonoUsuario: string;
  Usuario: string;
  Contrasena: string;
  IDRol: number;
  IDFactura: number;
  Cantidad: number = 0;
  CodigoFactura: string;
  CodigoPedido: string;
  Recargo: number;
  PagoT: boolean = true

  constructor( private emailService: EmailService, private estadosService: EstadosService, private pedidosService: PedidosService, private productosService: ProductosService ,private usuariosService: UsuariosService, private facturasService: FacturasService, private detallefacturasService: DetallefacturasService ) {   }

  ngOnInit() {
    this.setData();
    this.getProductos();
    this.getPedidos();
    this.getEstados();
    this.IDUsuario = AppComponent.UL[0].Usuario.IDUsuario
    this.carrito = TiendaComponent.carrito;
    this.SubTotal = TiendaComponent.Total;
    this.Total = this.SubTotal + (this.SubTotal * 5 ) / 100;
    this.Recargo = this.SubTotal * 0.05
  }

  getPedidos(){
    this.pedidosService.getPedidos().subscribe(res => {
      this.listaPedidos = res.json();
    });
  }  

  getEstados(){
    this.estadosService.getEstados().subscribe(res => {
      this.listaEstados = res.json();
    });
  }  

  getProductos(){
    this.productosService.getProductos().subscribe(res => {
      this.listaProductos = res.json();
    });
  }

  setData(){
    this.usuariosService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
      this.IDUsuario = AppComponent.UL[0].Usuario.IDUsuario
      this.Imagen = AppComponent.UL[0].Usuario.Imagen
      this.NombreUsuario = AppComponent.UL[0].Usuario.NombreUsuario
      this.ApellidoUsuario = AppComponent.UL[0].Usuario.ApellidoUsuario
      this.DireccionUsuario = AppComponent.UL[0].Usuario.DireccionUsuario
      this.CorreoUsuario = AppComponent.UL[0].Usuario.CorreoUsuario
      this.TelefonoUsuario = AppComponent.UL[0].Usuario.TelefonoUsuario
      this.Usuario = AppComponent.UL[0].Usuario.Usuario
      this.Contrasena = AppComponent.UL[0].Usuario.Contrasena
      this.IDRol = AppComponent.UL[0].Usuario.IDRol
    });
  }

  pago(){
    this.PagoT = false
  }

  pago2(){
    this.PagoT = true
  }

  postPedido(){
    this.CodigoPedido = "P000000" + (this.listaPedidos.length + 1)
    const data = {
      CodigoPedido: this.CodigoPedido,
      IDEstado: 3,
      IDUsuario: this.IDUsuario
    }       
    this.pedidosService.postPedidos(data).subscribe(res => {
      this.pedidosService.getPedidos().subscribe(res => {
        this.listaPedidos = res.json();
        this.facturasService.getFacturas().subscribe(res => {
          this.listaFacturas = res.json();
          for (let i = 0; i < this.listaFacturas.length; i++) {
            this.IDFactura = this.listaFacturas[i].IDFactura
          }
          this.CodigoFactura = "F000000" + (this.listaFacturas.length + 1)
          for (let i = 0; i < this.listaPedidos.length; i++) {
            this.IDPedido = this.listaPedidos[i].IDPedido
          }
          const data = {
            CodigoFactura: this.CodigoFactura,
            Total: this.Total,
            IDPedido: this.IDPedido
          };
          this.facturasService.postFacturas(data).subscribe(
            (datos) => {
              this.facturasService.getFacturas().subscribe(res => {
                this.listaFacturas = res.json();
                for (let i = 0; i < this.listaFacturas.length; i++) {
                  this.IDFactura = this.listaFacturas[i].IDFactura
                }
                for (let i = 0; i < this.carrito.length; i++) {
                  const data = {
                    IDProducto: this.carrito[i].IDProducto,
                    Cantidad: this.carrito[i].Cantidad,
                    SubTotal: this.carrito[i].Cantidad * this.carrito[i].Precio,
                    IDFactura: this.IDFactura
                  };
                  this.detallefacturasService.postDetalleFacturas(data).subscribe(
                    (datos) => {
                      for (let u = 0; u < this.listaProductos.length; u++) {
                        if (this.carrito[i].IDProducto == this.listaProductos[u].IDProducto) {
                          this.Cantidad = this.listaProductos[u].Cantidad
                          this.Cantidad = this.Cantidad - this.carrito[i].Cantidad
                          const data = {
                            Producto: this.listaProductos[u].Producto,
                            Precio: this.listaProductos[u].Precio,
                            Cantidad: this.Cantidad,
                            Estado: this.listaProductos[u].Estado,
                            IDCategoria: this.listaProductos[u].IDCategoria,
                            IDSubCategoria: this.listaProductos[u].IDSubCategoria,
                            IDProveedor: this.listaProductos[u].IDProveedor,
                            IDSucursal: this.listaProductos[u].IDSucursal
                          };
                          this.productosService.putProductos(this.carrito[i].IDProducto, data).subscribe(
                            (datos) => {
                              TiendaComponent.carrito = []
                              this.Comprado == true
                              setTimeout( () => { 
                                this.Comprado == false
                              }, 3000 );
                            }
                          );
                        }
                      }
                    }
                  );
                }
                this.estadoPedido();
              });
            }
          );
        });
      });
    });
  }

  estadoPedido(){
    this.IDEstado = this.listaPedidos[this.listaPedidos.length - 1].IDEstado
    for (let i = 0; i < this.listaEstados.length; i++) {
      if (this.IDEstado == this.listaEstados[i].IDEstado) {
        this.Estado = this.listaEstados[i].Estado
        this.Porcentaje = this.listaEstados[i].Porcentaje
      }
    }
    const data = {
      NombreUsuario: this.NombreUsuario,
      ApellidoUsuario: this.ApellidoUsuario,
      CorreoUsuario: AppComponent.UL[0].Usuario.CorreoUsuario,
      CodigoPedido: this.CodigoPedido,
      Estado: this.Estado,
      Porcentaje: this.Porcentaje
    }  
    this.emailService.emailPedido(data).subscribe(
      (datos) => {
        this.emailService.emailPedido2(data).subscribe(
          (datos) => {
            
        });
    });
  }

}
