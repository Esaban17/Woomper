import { Component, OnInit } from '@angular/core';
import { TiendaComponent } from '../tienda/tienda.component';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html'
})
export class CarritoComponent implements OnInit {

  carrito = [];
  Total: number;

  constructor() { }

  ngOnInit() {
    this.carrito = TiendaComponent.carrito;
    this.Total = TiendaComponent.Total;
  }

}
