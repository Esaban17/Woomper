import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from '../../services/autenticacion.service';
import { AppComponent } from '../../app.component';
import { MedallasService } from '../../services/medallas.service';
import { VerificarService } from '../../services/verificar.service';
import { LoginComponent } from '../login/login.component';
import { EmailService } from '../../services/email.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html'
})
export class RegistroComponent implements OnInit {

  list = [];
  Usuario: string;
  Contrasena: string;
  Contrasena2: string;
  NombreUsuario: string;
  ApellidoUsuario: string;
  DireccionUsuario: string
  TelefonoUsuario: string;
  CorreoUsuario: string;
  Codigo: string;
  Codig: string;

  Existe: boolean;
  Vacio: boolean;
  Fallo: boolean;
  Repetir: boolean;
  Verificar: boolean;

  cod: string
  user: string
  pass: string
  name: string
  lastname: string
  dire: string
  tel: string
  mail: string
  rol: number
  status: string
  id: number

  constructor( private autenticacionService: AutenticacionService, private verificarService: VerificarService, private emailService: EmailService ) { }

  ngOnInit( ) { }

  register(){
    if (this.Contrasena == this.Contrasena2) {
      this.Repetir = true;
      if ( this.NombreUsuario == null || this.Usuario == null || this.Contrasena == null || this.NombreUsuario == "" || this.Usuario == "" || this.Contrasena == "" ) {
        this.Vacio = true;
        this.Fallo = null;
      }else{
        this.Vacio = false;
        this.Codig = Math.floor((Math.random() * 9999) + 1000).toString();
        this.autenticacionService.register({"NombreUsuario":this.NombreUsuario, "ApellidoUsuario":this.ApellidoUsuario, "CorreoUsuario":this.CorreoUsuario ,"DireccionUsuario":this.DireccionUsuario,"TelefonoUsuario":this.TelefonoUsuario,"Usuario":this.Usuario, "Contrasena":this.Contrasena, "Estado": 0 ,"Codigo": this.Codig, "IDRol": 3}).subscribe(
          (data) => {
            if (JSON.parse(data._body).msg === "Usuario Existente") {
              this.Fallo = true;
              this.Existe = true;
            } else {
              this.Fallo = undefined;
              this.Existe = false;
              this.Verificar = true
              const data = {
                NombreUsuario: this.NombreUsuario,
                ApellidoUsuario: this.ApellidoUsuario,
                CorreoUsuario: this.CorreoUsuario,
                Codigo: this.Codig
              };
              this.emailService.emailBienvenido(data).subscribe(
                (datos) => {
                  this.emailService.emailVerificar(data).subscribe(
                    (datos) => {
                      this.emailService.emailNuevoUsuario(data).subscribe(
                        (datos) => {
                          
                        }
                      );
                    }
                  );
                }
              );
            }
          }
        );
      }
    }else{
      this.Repetir = false;
    }
  }

  login(){ 
    if ( this.Usuario == null || this.Contrasena == null || this.Usuario == "" || this.Contrasena == "" ) {
      this.Vacio = true;
      this.Fallo = undefined;
    }else{
      this.Vacio = false;
      this.autenticacionService.login({"Usuario":this.Usuario, "Contrasena":this.Contrasena}).subscribe(
        (data) => {          
          if (JSON.parse(data._body).msg === "No Encontrado") {
            this.Fallo = true;   
            sessionStorage.clear()      
          } else {
            AppComponent.UL[0] = JSON.parse(data._body);
            this.NombreUsuario = AppComponent.UL[0].Usuario.NombreUsuario
            this.id = AppComponent.UL[0].Usuario.IDUsuario
            this.user = AppComponent.UL[0].Usuario.Usuario
            this.pass = AppComponent.UL[0].Usuario.Contrasena
            this.cod = AppComponent.UL[0].Usuario.Codigo
            this.name = AppComponent.UL[0].Usuario.NombreUsuario
            this.lastname = AppComponent.UL[0].Usuario.ApellidoUsuario
            this.dire = AppComponent.UL[0].Usuario.DireccionUsuario
            this.tel = AppComponent.UL[0].Usuario.TelefonoUsuario
            this.mail = AppComponent.UL[0].Usuario.CorreoUsuario
            this.rol = AppComponent.UL[0].Usuario.IDRol
            if (AppComponent.UL[0].Usuario.Estado == 0) {
              this.Verificar = true
              AppComponent.UL[0] = null
              sessionStorage.clear();
            } else{
              LoginComponent.Hora = new Date().toLocaleTimeString();
              this.Fallo = false;
              this.Verificar = undefined
            }
          }
        }
      );
    }
  }

  verificar(){
    if ( this.cod == this.Codig) {
      const data = {
        NombreUsuario: this.name,
        ApellidoUsuario: this.lastname,
        DireccionUsuario: this.dire,
        CorreoUsuario: this.mail,
        TelefonoUsuario:this.tel,
        Contrasena: this.pass,
        Estado: 1,
        Codigo: this.cod,
        IDRol: this.rol
      };
      this.verificarService.putUsuarios(this.id, data).subscribe(
        (datos) => {
          this.Verificar = undefined
          this.Fallo = false
          this.login()
        }
      ); 
    }else{
      this.Verificar = false
      setTimeout( () => { 
        this.Verificar = true
      }, 3000 );
    }
  }

}
