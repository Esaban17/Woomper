import { Component, OnInit, DoCheck } from '@angular/core';
import { AppComponent } from '../../app.component';
import { TiendaComponent } from '../tienda/tienda.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, DoCheck {

  listaChats = [];
  listaFiltroChats = [];

  Mensaje: string

  APP: string = "Woomper"
  menu: string = "offcanvas-menu closed";
  menu2: string = "collapse navbar-collapse";
  abierto: boolean = false;

  Name: string;
  Image: string;
  ID: number;
  CantCarrito: number;
  CartVacio: boolean = true;
  Total: number;

  Acceso: boolean = false;
  Token: string = null;
  logo: boolean = true;
  static borrado: boolean;
  static producto: string;
  Existe: boolean
  carrito = [];
  Emisor: string

  constructor( private router: Router ) {  }

  ngOnInit() {
    var userAgent = navigator.userAgent || navigator.vendor;
    if (/iPad|iPhone|iPod/.test(userAgent)) {
      this.logo = false
    }else{
      this.logo = true
    }
    AppComponent.UL[0] = null
    if (AppComponent.UL[0] == null) {
      this.Acceso = false;
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.Acceso = true;
        this.Name = AppComponent.UL[0].Usuario.NombreUsuario
        this.Image = AppComponent.UL[0].Usuario.Imagen
        this.ID = AppComponent.UL[0].Usuario.IDRol
        this.carrito = TiendaComponent.carrito;
      }else{
        this.Acceso = false;
      }
    }
  }

  ngDoCheck(){
    var userAgent = navigator.userAgent || navigator.vendor;
    if (/iPad|iPhone|iPod/.test(userAgent)) {
      this.logo = false
    }else{
      this.logo = true
    }
    if (AppComponent.UL[0] == null) {
      this.Acceso = false;
    }else{
      this.Token = AppComponent.UL[0].Token
      if (this.Token == AppComponent.UL[0].Token) {
        this.Acceso = true;
        this.Name = AppComponent.UL[0].Usuario.NombreUsuario
        this.Image = AppComponent.UL[0].Usuario.Imagen
        this.ID = AppComponent.UL[0].Usuario.IDRol
        this.carrito = TiendaComponent.carrito;
        this.CantCarrito = TiendaComponent.carrito.length
        this.Total = TiendaComponent.Total
        if ( this.carrito.length == 0){
          this.CartVacio = true;
        }else{
          this.CartVacio = false;
        }
      }else{
        this.Acceso = false;
      }
    }
  }

  // chat(){
  //   this.chatsService.getChats().subscribe(res => {
  //     this.listaChats = res.json();
  //     for (let i = 0; i < this.listaChats.length; i++) {
  //       if ( this.listaChats[i].Emisor == this.Emisor ) {
  //         this.listaFiltroChats.push(this.listaChats[i])
  //       } 
  //     }
  //   });
  // }

  // getChats(){
  //   this.Existe = false
  //   this.chatsService.getChats().subscribe(res => {
  //     this.listaChats = res.json();
  //     for (let i = 0; i < this.listaChats.length; i++) {
  //       if ( this.listaChats[i].Emisor != 1 ) {
  //         for (let e = 0; e < this.listaFiltroChats.length; e++) {
  //           if (this.listaChats[i].Emisor == this.listaFiltroChats[e].Emisor ) {
  //             this.Existe = true
  //           }
  //         }
  //         if (this.Existe == false) {
  //           this.listaFiltroChats.push(this.listaChats[i])
  //         }
  //       } 
  //     }
  //   });
  // }

  // postChats(){
  //   this.listaFiltroChats = [];
  //   const data = {
  //     Emisor: AppComponent.UL[0].Usuario.IDUsuario,
  //     Receptor: 1,
  //     Mensaje: this.Mensaje,
  //   };
  //   this.chatsService.postChats(data).subscribe(
  //     (datos) => {
  //       this.Mensaje = null
  //       this.getChats()
  //     }
  //   );
  // }

  abrirmenu(){
    this.menu = "offcanvas-menu";
  }

  cerrarmenu(){
    this.menu = "offcanvas-menu closed";
  }

  abrirmenu2(){
    this.abierto = true;
    this.menu2 = "collapse navbar-collapse show";
  }

  cerrarmenu2(){
    this.abierto = false;
    this.menu2 = "collapse navbar-collapse";
  }

  EliminarItem( index: number, Cantidad: number, Precio: number, Producto: string ){
    TiendaComponent.Total = TiendaComponent.Total - (Cantidad * Precio);
    this.carrito.splice(index, 1);
    HeaderComponent.producto = Producto
    HeaderComponent.borrado = true;
    setTimeout( () => { 
      HeaderComponent.borrado = false;
    }, 3000 );
  }

  buscar(){
    this.router.navigate(['tienda']);
  }

  logout(){
    this.Token = AppComponent.UL[0].Token
    AppComponent.UL[0] = null
    sessionStorage.clear();
  }

}
