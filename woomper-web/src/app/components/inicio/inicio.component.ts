import { Component, OnInit } from '@angular/core';
import { TiendaService } from '../../services/tienda.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html'
})
export class InicioComponent implements OnInit {

  APP: string = "Woomper"
  listaCategorias = [];
  listaProductos = [];
  listaUsuarios = [];
  listaSucursales = [];

  filtro =[];

  constructor( private tiendaService: TiendaService ) { }

  ngOnInit() {
    this.getCategorias();
    this.getProductos();
    this.getSucursales();
    this.getUsuarios();
  }

  getCategorias(){
    this.tiendaService.getCategorias().subscribe(res => {
      this.listaCategorias = res.json();
    });
  }

  getProductos(){
    this.tiendaService.getProductos().subscribe(res => {
      this.listaProductos = res.json();
      for (let i = this.listaProductos.length - 1; i > this.listaProductos.length - 5; i--) {
        this.filtro.push(this.listaProductos[i]);
      }
    });
  }

  getUsuarios(){
    this.tiendaService.getCantUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
    });
  }

  getSucursales(){
    this.tiendaService.getSucursales().subscribe(res => {
      this.listaSucursales = res.json();
    });
  }

  getCategoria( id: number ){
    for (let i = 0; i < this.listaCategorias.length; i++) {
      if (this.listaCategorias[i].IDCategoria == id) {
        return this.listaCategorias[i].Categoria
      }
    }
  }

}
