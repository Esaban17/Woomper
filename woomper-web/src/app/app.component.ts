import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AutenticacionService } from './services/autenticacion.service';
import { Location } from '@angular/common';
import { LoginComponent } from './components/login/login.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  static UL = [];

  constructor( 
    private autenticacionService: AutenticacionService, 
    private router: Router 
  ){ }

  ngOnInit(){
    AppComponent.UL[0] = "null";
    if ( sessionStorage.getItem('user') != null && sessionStorage.getItem('pass') != null ) {
      try {
        this.autenticacionService.login({"Usuario":sessionStorage.getItem('user'), "Contrasena":sessionStorage.getItem('pass')}).subscribe(
          (data) => {          
            if (JSON.parse(data._body).msg === "No Encontrado") {     
            } else {
              AppComponent.UL[0] = JSON.parse(data._body);
              sessionStorage.setItem('user', AppComponent.UL[0].Usuario.Usuario)
              sessionStorage.setItem('pass', AppComponent.UL[0].Usuario.Contrasena)
              LoginComponent.Hora = new Date().toLocaleTimeString()
              console.log(this.router.url);
              this.router.navigate(['inicio']);
            }
          }
        ); 
      } catch (error) {
        
      }
    }
  }

}
