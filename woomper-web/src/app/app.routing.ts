import { RouterModule, Routes} from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { TiendaComponent } from './components/tienda/tienda.component';
import { HomeComponent } from './components/home/home.component';
import { RegistroComponent } from './components/registro/registro.component';
import { LoginComponent } from './components/login/login.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { PanelComponent } from './components/panel/panel.component';
import { CategoriasComponent } from './components/panel/categorias/categorias.component';
import { SubcategoriasComponent } from './components/panel/subcategorias/subcategorias.component';
import { SucursalesComponent } from './components/panel/sucursales/sucursales.component';
import { ProveedoresComponent } from './components/panel/proveedores/proveedores.component';
import { ProductosComponent } from './components/panel/productos/productos.component';
import { PedidosComponent } from './components/panel/pedidos/pedidos.component';
import { UsuariosComponent } from './components/panel/usuarios/usuarios.component';
import { CajaComponent } from './components/caja/caja.component';
import { CarritoComponent } from './components/carrito/carrito.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { FaqsComponent } from './components/faqs/faqs.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { AyudaComponent } from './components/ayuda/ayuda.component';

const APP_ROUTES: Routes = [
    //Home
    { path: '', component: InicioComponent },
    { path: 'landing', component: HomeComponent },
    { path: 'inicio', component: InicioComponent },
    { path: 'tienda', component: TiendaComponent },
    { path: 'registro', component: RegistroComponent },
    { path: 'login', component: LoginComponent },
    { path: 'contacto', component: ContactoComponent },
    { path: 'faqs', component: FaqsComponent },
    { path: 'nosotros', component: NosotrosComponent },
    { path: 'ayuda', component: AyudaComponent },
    
    //Administrador
    { path: 'panel', component: PanelComponent },
    { path: 'panel/categorias', component: CategoriasComponent },
    { path: 'panel/subcategorias', component: SubcategoriasComponent },
    { path: 'panel/sucursales', component: SucursalesComponent },
    { path: 'panel/proveedores', component: ProveedoresComponent },
    { path: 'panel/productos', component: ProductosComponent },
    { path: 'panel/pedidos', component: PedidosComponent },
    { path: 'panel/usuarios', component: UsuariosComponent },
    //Usuario
    { path: 'perfil', component: PerfilComponent },
    { path: 'carrito', component: CarritoComponent },
    { path: 'caja', component: CajaComponent },
    //Error 404
    // { path: '**', component: Error404Component }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);